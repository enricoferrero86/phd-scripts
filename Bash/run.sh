#!/bin/sh

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

# sort all BED files
for i in *.bed
do
	bedtools sort -i $i > ${i%.bed}.sorted.bed
done

# replace non-sorted with sorted BED files
rename -f 's/.sorted//' *.sorted.bed

# create unique.transcomp datasets
bedtools intersect -a SoxNDam.unique.bed -b SoxN-DDam.transcomp.bed > SoxNDam.unique.Dtranscomp.bed
bedtools intersect -a DDam.unique.bed -b D-SoxNDam.transcomp.bed > DDam.unique.SoxNtranscomp.bed

# create unique.notranscomp datasets
bedtools intersect -v -a SoxNDam.unique.bed -b SoxN-DDam.transcomp.bed > SoxNDam.unique.noDtranscomp.bed
bedtools intersect -v -a DDam.unique.bed -b D-SoxNDam.transcomp.bed > DDam.unique.noSoxNtranscomp.bed

# create results files
for i in CNS.all.bed GBE.CNS.all.bed GBE.NBs.all.bed GBE.Neurons.all.bed St16.CNS.all.bed Midline.all.bed
do
	for j in D-SoxNDam.nochange.bed D-SoxNDam.denovo.bed D-SoxNDam.loss.bed D-SoxNDam.overcomp.bed D-SoxNDam.transcomp.bed SoxNDam.unique.bed SoxNDam.unique.noDtranscomp.bed SoxNDam.unique.Dtranscomp.bed SoxN-DDam.nochange.bed SoxN-DDam.denovo.bed SoxN-DDam.loss.bed SoxN-DDam.overcomp.bed SoxN-DDam.transcomp.bed DDam.unique.bed DDam.unique.noSoxNtranscomp.bed DDam.unique.SoxNtranscomp.bed SoxNDam.DDam.common.bed
	do
		bedtools intersect -a $i -b $j > ${i%.bed}.in.${j%.bed}.bed
	done
done

# create list of enhancers hit by each dataset
for i in *.in.*.bed
do
	cut -f4 $i | sort | uniq > ${i%.bed}.enhancers.txt
done

# create list of genes whose enhancers are hit by each dataset
for i in *.in.*.bed
do
	cut -f4 $i | cut -d. -f1 | sort | uniq > ${i%.bed}.genes.txt
done
