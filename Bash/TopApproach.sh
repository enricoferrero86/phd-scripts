#!/bin/sh

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

# create BED5 files from SGR and FDR5.BED3 files

for i in DDam SoxNDam SoxN-DDam D-SoxNDam
	do bed5.creator.avg.pl ${i}.sgr ${i}.FDR5.bed3 > ${i}.FDR5.bed
done

# rank BED5 files by score
for i in DDam SoxNDam SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.FDR5.bed > ${i}.FDR5.byScore.bed
done

# select top 2000 intervals
for i in DDam SoxNDam SoxN-DDam D-SoxNDam
	do head -n 2000 ${i}.FDR5.byScore.bed > ${i}.top2000.byScore.bed
done

# rank top2000 BED files by name
for i in DDam SoxNDam SoxN-DDam D-SoxNDam;
	do bedtools sort -i ${i}.top2000.byScore.bed > ${i}.top2000.byName.bed
done

# make a copy without the top2000 for next operations
for i in DDam SoxNDam SoxN-DDam D-SoxNDam;
	do cp ${i}.top2000.byName.bed ${i}.byName.bed
done
for i in DDam SoxNDam SoxN-DDam D-SoxNDam;
	do cp ${i}.top2000.byScore.bed ${i}.byScore.bed
done

# compute SoxNDam unique intervals (intervals in SoxNDam but not DDam)
bedtools subtract -A -a SoxNDam.byName.bed -b DDam.byName.bed > SoxNDam.unique.byName.bed
# compute DDam unique intervals (intervals in DDam but not SoxNDam)
bedtools subtract -A -a DDam.byName.bed -b SoxNDam.byName.bed > DDam.unique.byName.bed
# compute D-SoxNDam unique intervals (intervals in D-SoxNDam but not SoxNDam)
bedtools subtract -A -a D-SoxNDam.byName.bed -b SoxNDam.byName.bed > D-SoxNDam.unique.byName.bed
# compute SoxN-DDam unique intervals (intervals in SoxN-DDam but not DDam)
bedtools subtract -A -a SoxN-DDam.byName.bed -b DDam.byName.bed > SoxN-DDam.unique.byName.bed
# rank the resulting unique files by score
for i in DDam SoxNDam SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.unique.byName.bed > ${i}.unique.byScore.bed
done

# Assess transcompensation:
# SoxN binding in place of D in D mutants
bedtools intersect -a DDam.byName.bed -b D-SoxNDam.byName.bed > DDam.D-SoxNDam.common.byName.bed
bedtools subtract -A -a DDam.D-SoxNDam.common.byName.bed -b SoxNDam.byName.bed > D-SoxNDam.transcomp.byName.bed
# D binding in place of SoxN in SoxN mutants
bedtools intersect -a SoxNDam.byName.bed -b SoxN-DDam.byName.bed > SoxNDam.SoxN-DDam.common.byName.bed
bedtools subtract -A -a SoxNDam.SoxN-DDam.common.byName.bed -b DDam.byName.bed > SoxN-DDam.transcomp.byName.bed
# rank the resulting transcomp files by score
for i in SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.transcomp.byName.bed > ${i}.transcomp.byScore.bed
done

# Assess overcompensation:
# SoxN increases binding in its own locations in D mutants
bed5.intersect.overcomp.pl D-SoxNDam.byName.bed SoxNDam.byName.bed > D-SoxNDam.overcomp.byName.bed
# D increases binding in its own locations in SoxN mutants
bed5.intersect.overcomp.pl SoxN-DDam.byName.bed DDam.byName.bed > SoxN-DDam.overcomp.byName.bed
# rank the resulting overcomp files by score
for i in SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.overcomp.byName.bed > ${i}.overcomp.byScore.bed
done

# Assess random compensation:
# SoxN binds to new locations in D mutants
bedtools subtract -A -a D-SoxNDam.unique.byName.bed -b D-SoxNDam.transcomp.byName.bed > D-SoxNDam.randcomp.byName.bed
# D binds to new locations in SoxN mutants
bedtools subtract -A -a SoxN-DDam.unique.byName.bed -b SoxN-DDam.transcomp.byName.bed > SoxN-DDam.randcomp.byName.bed
# rank the resulting randcomp files by score
for i in SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.randcomp.byName.bed > ${i}.randcomp.byScore.bed
done

# Assess loss:
# SoxN looses binding in D mutants
bedtools subtract -A -a SoxNDam.byName.bed -b D-SoxNDam.byName.bed > D-SoxNDam.loss.byName.bed
# D looses binding in SoxN mutants
bedtools subtract -A -a DDam.byName.bed -b SoxN-DDam.byName.bed > SoxN-DDam.loss.byName.bed
# rank the resulting loss files by score
for i in SoxN-DDam D-SoxNDam
	do sort -r -k5 ${i}.loss.byName.bed > ${i}.loss.byScore.bed
done

# remove unnecessary files
rm *common*.bed
rm *top2000*.bed
rm *FDR5*.bed
rm *byName*.bed




















