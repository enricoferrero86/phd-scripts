#!/bin/sh

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

#!/bin/bash

# Before running, sort your BED files with sort-bed (bedops) or sortBed (bedtools) or IntervalStats will segfaults!
for i in SoxN*.bed; do
	for j in SoxN*.bed; do
		echo -e "\n\nWorking on REFERENCE: $i; QUERY: $j"
		IntervalStats -d Chromosomes.bed -r $i -q $j -o R.${i%.bed}_Q.${j%.bed}.txt
	done
done

exit
