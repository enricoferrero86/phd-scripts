#!/bin/sh

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

cat St16.Brain.bed St16.CNS.deep.layer.bed St16.CNS.large.subset.bed st16.CNS.scattered.bed St16.CNS.small.subset.bed St16.CNS.superficial.layer.bed St16.EVE.Lateral.Cells.bed St16.EVE.medial.cells.bed St16.Midline.bed | bedtools sort -i | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/St16.CNS.all.bed

cat GBE.head.region.bed GBE.Midline.bed GBE.NB.all.bed GBE.NB.subset.bed GBE.Neurons.many.all.bed GBE.neuron.subset.bed | bedtools sort -i | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/GBE.CNS.all.bed

cat GBE.NB.all.bed GBE.NB.subset.bed | bedtools sort -i | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/GBE.NBs.all.bed

cat GBE.Neurons.many.all.bed GBE.neuron.subset.bed | bedtools sort -i | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/GBE.Neurons.all.bed

cat St16.Midline.bed GBE.Midline.bed | bedtools sort -i | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/Midline.all.bed

cat Datasets/GBE.CNS.all.bed Datasets/St16.CNS.all.bed | bedtools sort -i  | sort -k1,1 -k2,2n -k3,3n -k4,4 -k5,5n | uniq > Datasets/CNS.all.bed

