#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %intervals;
my %scores;
my %scorers;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 window_scores_sgr_file.sgr bed_file.bed > scores_bed_file.bed5\n";
	exit;
}

my $sgrfile = $ARGV[0];
my $bedfile = $ARGV[1];
(my $name = $sgrfile) =~ s/(.+)\..+/$1/; 

open my $BEDFILE, "<", $bedfile or die "Can't open $bedfile: $!";
while (my $line = <$BEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $coord = $start . "\t" . $end;
	foreach my $nt ($start .. $end) {
		$intervals{$chr}{$coord}{$nt} = 1; 
	}
}
close $BEDFILE;

open my $SGRFILE, "<", $sgrfile or die "Can't open $sgrfile: $!";
while (my $line = <$SGRFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $nt = $tmp[1];
	my $score = $tmp[2];
	$scores{$chr}{$nt} = $score;
}
close $SGRFILE;

foreach my $chr (keys %intervals) {
	foreach my $coord (keys %{$intervals{$chr}}) {
		my $sum = 0;
		my $counter = 0;
		foreach my $nt (keys %{$intervals{$chr}{$coord}}) {
			if (exists $scores{$chr}{$nt}) {
				my $score = $scores{$chr}{$nt};
				$sum += $score;
				$counter++;
			}
		}
		my $avgscore = $sum/$counter;
		$scorers{$chr}{$coord} = $avgscore;
	}
}

foreach my $chr (keys %scorers) {
	foreach my $coord (keys %{$scorers{$chr}}) {
		my $score = $scorers{$chr}{$coord};
		print $chr . "\t" . $coord . "\t" . $name . "\t" . $score . "\n";
	}
}
