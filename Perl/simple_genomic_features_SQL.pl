#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use DBI;

sub run_query {
  my $SQL_statement = $_[0];
  $_[2] = $_[1]->prepare($SQL_statement);
  $_[2]->execute() or die("fucking hell, your SQL was wrong");
}

my $database_handle1 = DBI->connect("DBI:mysql:enrico:127.0.0.1:7777", "enrico", "enricoSQL");
my $st;
my $st2;

my $genic = 0;
my $exonic = 0;
my $intronic = 0;
my $intergenic = 0;
my $exonic_intronic = 0;
my $total = 0;

unless ($ARGV[0]) {
	print "USAGE: $0 sbed_file.sbed dataset_name > genomic_features.txt\n";
	exit;
}

my $sbedfile = $ARGV[0];
my $dataset = $ARGV[1];

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $nt = $tmp[3];
	
	$total++;
	
	my $genic_count = 0;
	my $exonic_count = 0;
	my $intronic_count = 0;
	
	run_query qq(SELECT chrom, txStart, txEnd, exonStarts, exonEnds FROM UCSC_dm3.flyBaseGene WHERE chrom="$chr" AND ($nt BETWEEN txStart AND txEnd)), $database_handle1, $st2;	
	while (my ($chrom, $txStart, $txEnd, $exonStarts, $exonEnds) = $st2->fetchrow_array) {
		$genic_count++;
		my $exonic_temp = 0;
		my @exon_starts = split(/,/, $exonStarts);
		my @exon_ends = split(/,/, $exonEnds);
		
		foreach my $i (0 .. $#exon_starts) {
			if ($exon_starts[$i] <= $nt and $nt <= $exon_ends[$i]) {
				$exonic_temp++;
			}
		}
		if ($exonic_temp > 0) {
			$exonic_count++;
		}
		else {
			$intronic_count++;
		}

	}
	
	if ($exonic_count > 0 and $intronic_count == 0) {
		$exonic++;
	}
	elsif ($exonic_count == 0 and $intronic_count > 0) {
		$intronic++;
	}
	elsif ($exonic_count > 0 and $intronic_count > 0) {
		$exonic_intronic++;
	}
	else {}
	
	
	if ($genic_count > 0) {
		$genic++;
	}
	else {
		$intergenic++;
	}
}

my $genic_perc = ($genic / $total) * 100;
my $intergenic_perc = ($intergenic / $total) * 100;
my $exonic_perc = ($exonic / $total) * 100;
my $intronic_perc = ($intronic / $total) * 100;
my $exonic_intronic_perc = ($exonic_intronic / $total) * 100;

printf "#Genomic features for $sbedfile ($total):\n#genic: %.2f%% ($genic)\n#intergenic: %.2f%% ($intergenic)\n#exonic: %.2f%% ($exonic)\n#intronic: %.2f%% ($intronic)\n#exonic_intronic: %.2f%% ($exonic_intronic)\n\n", $genic_perc, $intergenic_perc, $exonic_perc, $intronic_perc, $exonic_intronic_perc;
print "${dataset}_genic <- $genic\n${dataset}_intergenic <- $intergenic\n${dataset}_exonic <- $exonic\n${dataset}_intronic <- $intronic\n${dataset}_exonic_intronic <- $exonic_intronic\n";
		
		
		
		
