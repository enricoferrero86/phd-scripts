#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

use strict;
use warnings;

# Generic variables
my $window;
my $count = 0;
my %genes;

# BED file variables
my $bedfile;
my %bedlines;
my @bedfields;
my $bedchr;
my $bedstart;
my $bedend;

# REF file variables
my $reffile;
my %reflines;
my @reffields;
my $refchr;
my $refstart;
my $refend;
my $fbgn;

# Check for arguments
unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 bed_file.bed flymine_ref_file.tsv [window]\n";
	print "INFO: window default is 0 kb\n";
	exit;
}
$bedfile = $ARGV[0];
$reffile = $ARGV[1];

if ($ARGV[2]) {
	$window = $ARGV[2];
}
else {
	$window = 0;
}

# Read bedfile into an hash
open BEDFILE, $bedfile or die "Can't open $bedfile: $!";
while (my $bedline = <BEDFILE>) {
	$bedlines{$bedline}++;
}
close BEDFILE;

# Read reffile line by line and compare it to the bedfile hash as it go through it
open REFFILE, $reffile or die "Can't open $reffile: $!";
while (my $refline = <REFFILE>) {
	$count++;
	unless ($count <= 1) {
		@reffields = split /\t/, $refline;
		$refchr = "chr" . $reffields[0];
		$refstart = $reffields[2] - $window;
		$refend = $reffields[3] + $window;
		$fbgn = $reffields[5];

		foreach my $bedline (keys %bedlines) {
			@bedfields = split /\t/, $bedline;
			$bedchr = $bedfields[0];
			$bedstart = $bedfields[1];
			$bedend = $bedfields[2];
			
			if ($bedchr eq $refchr) {
				if (($bedstart >= $refstart && $bedstart <= $refend) ||
					($bedend >= $refstart && $bedend <= $refend) ||
					($refstart >= $bedstart && $refstart <= $bedend) ||
					($refend >= $bedstart && $refend <=$bedend)) {
					$genes{$fbgn}++;
				}
			}
		}
	}
}
close REFFILE;

foreach (keys %genes) {
	print $_, "\n";
}					
			
			
	
	
	
