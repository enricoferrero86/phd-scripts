#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my $alpha;
my $logFCpos;
my $logFCneg;

my %tablines;

my $FBgn;
my $logFC;
my $pvalue;

my %diff;
my %up;
my %down;


unless ($ARGV[0]) {
	print "USAGE: $0 Rxxxxx.norm.pack.fdr.tab [p-value cutoff] [positive log(FoldChange)] [negative log(FoldChange)]\n";
	print "Default values are: p-value cutoff = 0.05; positive log(FoldChange) = 0.5; negative log(FoldChange) = -0.5\n";
	exit;
}

if ($ARGV[1] and $ARGV[2] and $ARGV[3]) {
	$alpha = $ARGV[1];
	$logFCpos = $ARGV[2];
	$logFCneg = $ARGV[3];
}
else {
	$alpha = 0.05;
	$logFCpos = 0.5;
	$logFCneg = -0.5;
}
	
my $tabfile = $ARGV[0];
open my $TABFILE, "<", $tabfile or die "Can't open $tabfile: $!";
while (my $tabline = <$TABFILE>) {
	next if $. <= 2; # ignore first two lines
	chomp $tabline;
	$tablines{$tabline}++;
}
close $TABFILE;

foreach my $tabline (keys %tablines) {
	my @tabfields = split /\t/, $tabline;
	($FBgn = $tabfields[10]) =~ s/"//g; # get rid of " (if any)
	($logFC = $tabfields[25]) =~ s/"//g;
	($pvalue = $tabfields[28]) =~ s/"//g;
	
	if ($pvalue <= $alpha) {
		if ($logFC <= $logFCneg) {
			$down{$FBgn}++;
			$diff{$FBgn}++;
		}
		elsif ($logFC >= $logFCpos) {
			$up{$FBgn}++;
			$diff{$FBgn}++;
		}
	}
}

my $diff = keys %diff;
my $up = keys %up;
my $down =  keys %down;

print "In $tabfile I found $diff differentially expressed genes (p-value < $alpha), $down downregulated (negative log(FoldChange) < $logFCneg), $up upregulated (positive log(FoldChange) > $logFCpos).\n";

(my $downfile = $tabfile) =~ s/(.+)\..+/$1\_$alpha\_$logFCneg\_DOWN\.txt/;
(my $upfile = $tabfile) =~ s/(.+)\..+/$1\_$alpha\_$logFCpos\_UP\.txt/;
(my $difffile = $tabfile) =~ s/(.+)\..+/$1\_$alpha\_$logFCpos\_$logFCneg\_DIFF\.txt/;


open my $DOWNFILE, ">", $downfile or die "Can't open $downfile: $!";
foreach (keys %down) {
	print $DOWNFILE "$_\n";
}
close $DOWNFILE;

open my $UPFILE, ">", $upfile or die "Can't open $upfile: $!";
foreach (keys %up) {
	print $UPFILE "$_\n";
}
close $UPFILE;

open my $DIFFFILE, ">", $difffile or die "Can't open $difffile: $!";
foreach (keys %diff) {
	print $DIFFFILE "$_\n";
}
close $DIFFFILE;
