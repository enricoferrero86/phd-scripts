#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use Data::Dumper;

unless (scalar(@ARGV) == 3) {
	print "USAGE: $0 FlyMine_transcript_ref.tsv TF_sbedfile.sbed dataset_name\n";
	exit;
}

my $reffile = $ARGV[0];
my $sbedfile = $ARGV[1];
my $data = $ARGV[2];

my %trs;
my %l_trs;

my %all;
my %bound;

open FH, "<", $reffile or die "Can't open $reffile: $!";
readline(FH);
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $gn = $tmp[0];
	my $tr = $tmp[1];
	my $length = $tmp[2];
	my $chr = "chr" . $tmp[3];
	my $start = $tmp[4];
	my $end = $tmp[5];
	
	my $coord = $start . "\t" . $end;
	$trs{$gn}{$tr}{$chr}{$coord} = $length;
}
close FH;

foreach my $gn (keys %trs) {
	my $tr = "";
	my $chr = "";
	my $coord = "";
	my $length = 0;
	my $l_tr = "";
	my $l_chr = "";
	my $l_coord = "";
	my $l_length = 0;
	foreach $tr (keys %{$trs{$gn}}) {
		foreach $chr (keys %{$trs{$gn}{$tr}}) {
			foreach $coord (keys %{$trs{$gn}{$tr}{$chr}}) {
				$length = $trs{$gn}{$tr}{$chr}{$coord};

				if ($length > $l_length) {
					$l_tr = $tr;
					$l_chr = $chr;
					$l_coord = $coord;
					$l_length = $length;
				}
			}
		}
	}
	$l_trs{$gn}{$l_tr}{$l_chr}{$l_coord} = $l_length;
	$all{$gn} = $l_length;
}


open FH, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	
	foreach my $gn (keys %l_trs) {
		foreach my $tr (keys %{$l_trs{$gn}}) {
			foreach my $tr_chr (keys %{$l_trs{$gn}{$tr}}) {
				foreach my $tr_coord (keys %{$l_trs{$gn}{$tr}{$tr_chr}}) {
					my $length = $l_trs{$gn}{$tr}{$tr_chr}{$tr_coord};
					my @tmp = split /\t/, $tr_coord;
					my $tr_start = $tmp[0];
					my $tr_end = $tmp[1];
					
					if ($chr eq $tr_chr) {
						if ($nt > $tr_start and $nt < $tr_end) {
							$bound{$gn} = $length;
						}
					}
				}
			}
		}
	}
}
close FH;

print "${data}_ALL <- c(";
foreach my $gn (keys %all) {
	my $length = $all{$gn};
	print "$length, ";
}
print ")\n";

print "${data}_BOUND <- c(";
foreach my $gn (keys %bound) {
	my $length = $bound{$gn};
	print "$length, ";
}
print ")\n";

print "${data}_UNBOUND <- ${data}_ALL[-${data}_BOUND]\n";


