#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use Data::Dumper;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 [gs]bedfile.[gs]bed dataset_name > results.R\n";
	exit;
}

my $sbedfile = $ARGV[0];
my $dataset = $ARGV[1];

my %scores;
my $score;

my %intervals;
my $int_count;
my $int_score_sum;
my $int_score_avg;
my %windows;
my $win_count;
my $win_score_sum;
my $win_score_avg;

my $limit = 5000;
my $win_size = 100;


open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line=<$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	
	my $l_limit = $nt - $limit;
	my $r_limit = $nt + $limit;
	my $limits = $l_limit . "\t" . $r_limit;
	
	$intervals{$chr}{$limits}++;
}


my $path_to_scores = "/flychip/share/Genomes/UCSCmirror/dm3/phastCons/";
my @chrom = ("chr2L", "chr2LHet", "chr2R", "chr2RHet", "chr3L", "chr3LHet", "chr3R", "chr3RHet", "chr4", "chrX", "chrXHet", "chrYHet", "chrM", "chrU", "chrUextra");

foreach my $chr (@chrom) {
	my $pos;
	open my $FILE, $path_to_scores.$chr.".pp" or die "Can't open $path_to_scores.$chr.pp: $!";
	while (my $line = <$FILE>) {
		chomp $line;
		if ($line =~ /start=(\d+)/) {
			$pos = $1;
		}
		else {
			$scores{$chr}{$pos} = $line;
			$pos++;
		}
	}
}

#~ print "\nDEBUG\n\n";
#~ foreach my $chr (keys %scores) {
	#~ foreach my $pos (sort {$a <=> $b} keys %{$scores{$chr}}) {
		#~ my $score = $scores{$chr}{$pos};
		#~ print $chr . "\t" . $pos . "\t" . $score . "\n";
	#~ }
#~ }
 

foreach my $chr (keys %intervals) {
	foreach my $limits (keys %{$intervals{$chr}}) {
		
		$int_count++;
		
		my @tmp = split /\t/, $limits;
		my $l_limit = $tmp[0];
		my $r_limit = $tmp[1];
		
		my $win_start = $l_limit;
		my $win_end = $win_start + $win_size;
		$win_count = 0;
		
		while ($win_end <= $r_limit) {
			$win_score_sum = 0;
			foreach my $nt ($win_start .. $win_end) {
				if (exists $scores{$chr}{$nt}) {
					$score = $scores{$chr}{$nt};
				}
				else {
					$score = 0;
				}
				$win_score_sum += $score;
			}
			$win_score_avg = $win_score_sum / $win_size;
			$windows{$win_count}{$win_score_avg}++;
			$win_start = $win_end;
			$win_end = $win_start + $win_size;
			$win_count++;
		}
	}
}

#~ print "\nDEBUG\n\n";
#~ print Dumper \%windows;
#~ print "\n\n";
#~ print $win_count, "\n\n";


print "\n$dataset <- c(";
foreach my $win_count (sort {$a <=> $b} keys %windows) {
	$int_score_sum = 0;
		foreach my $win_score_avg (keys %{$windows{$win_count}}) {
			my $n = $windows{$win_count}{$win_score_avg};
			my $temp = $n * $win_score_avg;
			$int_score_sum += $temp;
		}
	$int_score_avg = $int_score_sum / $int_count;
	print "$int_score_avg, ";
}
print ")\n\n";	
