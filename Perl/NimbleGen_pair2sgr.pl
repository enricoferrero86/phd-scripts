#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero, Bettina Fischer.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# Original code by Bettina Fischer, modified by Enrico Ferrero.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


# Removes # header row to make files compatible with IGB and replaces HET with Het
# Extracts the chrmomosome location, oligo start position and ratio column from *.pair files
# Bettina Fischer, 20110610

use strict;

my $infile = $ARGV[0];

if ($infile !~ m/\w+/) {
    die "\nUsage: pair2sgr.pl inputfile.pair > outputfile.sgr\n\n";
}

open (INFILE, "$infile") or die "Cannot open $infile!\n\n";

while(<INFILE>) {
    chomp $_;
    s/HET/Het/g;
    if ($_ =~ m/^\#/) { next } # removes the Nimblegen first line (incompatible with IGB)
    if ($_ =~ m/IMAGE_ID/) { next }

    my @tabs = split (/\t/,$_);

    # need to extract the chromosome name from the column content (removes also random oligos)
    if ($tabs[2] =~ m/(^chr\w+):1-\d+/) {
	my $chr = $1;
	print "$chr\t$tabs[4]\t$tabs[9]\n";
    }
}
