#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0]) {
	print "USAGE: $0 bedfile.bed > gff3file.gff3\n";
	exit;
}

my $bedfile = $ARGV[0];

open FH, "<", $bedfile or die;
while (my $line =<FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	print $chr . "\t" . "NA" . "\t" . "NA" . "\t" . $start . "\t" . $end . "\t" . "1.00" . "\t" . "." . "\t" . "." . "\t" . "NA" . "\n";
}
close FH
