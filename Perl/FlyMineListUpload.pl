#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use Webservice::InterMine;

my $token = "b1P3A189U4v820gdjce1L4Ebjqt";
my $service = get_service("www.flymine.org/query/service", $token);

my $path = `pwd`;
chomp $path;
my @files = glob "*geneList.txt";

foreach my $file (@files) {
	my $list = $service->list(substr($file, -13);
	my $new_list = $service->new_list(type => "Gene", content => "$path/$file");
}

