#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0]) {
	print "USAGE: $0 input_gff3_file.gff3 > output_gff3_file.gff3\n";
	exit;
}

my $gff3file = $ARGV[0];

open GFF3FILE, "<", $gff3file or die "Can't open $gff3file: $!";
while (my $line = <GFF3FILE>) {
	chomp $line;
	if ($line =~ /interval/i) {
		print "$line\n";
	}
}
close GFF3FILE;

