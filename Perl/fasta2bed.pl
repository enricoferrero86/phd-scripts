#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %headers;
my $chr;
my $start;
my $end;

unless ($ARGV[0]) {
	print "USAGE: $0 fastafile.fasta\n";
	exit;
}
my $fastafile = $ARGV[0];

open FASTAFILE, $fastafile or die "Can't open $fastafile: $!";
while (my $fastaline = <FASTAFILE>) {
	chomp $fastaline;
	if ($fastaline =~ /^>/) {
		$headers{$fastaline}++;
	}
}
close FASTAFILE;

#~ foreach (keys %headers) {
	#~ print "$_\n";
#~ }

foreach my $header (sort keys %headers) {
	if ($header =~ /^>(chr.+):(\d+)-(\d+)\s*$/) {
		$chr = $1;
		$start = $2;
		$end = $3;
		print "$chr\t$start\t$end\n";
	}

}


