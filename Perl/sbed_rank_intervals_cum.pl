#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %intervals;

unless ($ARGV[0]) {
	print "USAGE: $0 sbed_file.sbed\n";
	exit;
}

my $sbedfile = $ARGV[0];

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	my $all = $chr . "\t" . $start . "\t" . $end . "\t" . $nt . "\t" . $score;
	$intervals{$all} = $score;
}
close $SBEDFILE;

my @ordered = sort {$intervals{$b} <=> $intervals{$a}} keys %intervals;

(my $rankedsbedfile = $sbedfile) =~ s/(.+)\.sbed/$1_RANKED\.sbed/;
open my $RANKEDSBEDFILE, ">", $rankedsbedfile or die "Can't open $rankedsbedfile; $!";
foreach my $interval (@ordered) {
	print $RANKEDSBEDFILE "$interval\n";
}
close $RANKEDSBEDFILE;



