#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %intervals;

unless ($ARGV[0]) {
	print "USAGE: $0 gff3_file.gff3 > scores_bed_file.gbed\n";
	exit;
}

my $gff3file = $ARGV[0];

open GFF3FILE, "<", $gff3file or die "Can't open $gff3file: $!";
while (my $line = <GFF3FILE>) {
	chomp $line;
	next if $line =~ /^\s*$/;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $type = $tmp[2];
	my $start = $tmp[3];
	my $end = $tmp[4];
	my $score = $tmp[5];
	my $coord = $start . "\t" . $end;
	my $midpoint = int (($start + $end) / 2);
	if ($type eq "Interval") {
		$intervals{$chr}{$coord}{$midpoint} = $score;
	}
}
close GFF3FILE;

foreach my $chr (keys %intervals) {
	foreach my $coord (keys %{$intervals{$chr}}) {
		foreach my $midpoint (keys %{$intervals{$chr}{$coord}}) {
			my $score = $intervals{$chr}{$coord}{$midpoint};
			print $chr . "\t" . $coord . "\t" . $midpoint . "\t" . $score . "\n";
		}
	}
}
 
