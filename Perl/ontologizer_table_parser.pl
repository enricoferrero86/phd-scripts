#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use Data::Dumper;

unless (scalar(@ARGV) >= 3) {
	print "USAGE: $0 GO_ontologizer_tables.txt GO_reference.txt subontology > results.txt\n";
	print "Multiple files can be used as input files\n";
	print "The GO_reference must be in the format GO_term_identifier	GO_term_name";
	print "'subontology' is usually something like biological_process', 'molecular_function' or 'cellular_component'\n";
	exit;
}

my $subontology = pop @ARGV;
my $go_ref = pop @ARGV;

my %all_terms;
my %sig_terms;
my %datasets;

open my $GO_REF, "<", $go_ref or die "Can't open $go_ref: $!";
while (my $line = <$GO_REF>) {
	my @tmp = split /\t/, $line;
	my $go_id = $tmp[0];
	my $go_name = $tmp[1];
	my $go_par_name = $tmp[2];
	
	if ($go_par_name =~ /^$subontology$/) {
	$all_terms{$go_id} = $go_name;
	}
}

#~ print Dumper \%all_terms;

foreach my $file (@ARGV) {
	(my $dataset = $file) =~ s/(.+)\..+/$1/;
	open my $FILE, "<", $file or die "Can't open $file: $!";
	readline $FILE;
	while (my $line = <$FILE>) {
		my @tmp = split /\t/, $line;
		my $go_id = $tmp[0];
		my $pvalue = $tmp[10];
		if ($pvalue < 0.05)  {
			$datasets{$dataset}++;
			$sig_terms{$go_id}{$dataset} = $pvalue;
		}
	}
}

#~ print Dumper \%sig_terms;

print "GO Term\t";
foreach my $dataset (sort keys %datasets) {
	print "$dataset\t";
}
print "\n";	

foreach my $go_id (keys %sig_terms) {
	if (exists $all_terms{$go_id}) {
		my $go_name = $all_terms{$go_id};
		print "$go_name\t";
		foreach my $dataset (sort keys %datasets) {
			my $pvalue = 1;
			if (exists $sig_terms{$go_id}{$dataset}) {
				$pvalue = $sig_terms{$go_id}{$dataset};
			}
			print "$pvalue\t";
		}
		print "\n";
	}
}
