#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;


# Declare variables
my $maxdist;
my %genes;
my %fdr1;
my %fdr10;
my %fdr1_hits;
my %fdr1_unresolved;
my %fdr10_hits;
my %fdr10_unresolved;
my %multipeaks_genes;
my %singlepeak_genes;
my %unclassified;


# Assign arguments
if (@ARGV < 2) {
	print "USAGE: $0 fdr1_bedfile.bed fdr10_bedfile.bed flymine_reffile.tsv [maximum distance]\n";
	print "INFO: maximum distance default is 10 kb\n";
	exit;
}

my $fdr1_bedfile = $ARGV[0];
my $fdr10_bedfile = $ARGV[1];
my $reffile = $ARGV[2];

if ($ARGV[3]) {
	$maxdist = $ARGV[3];
}
else {
	$maxdist = 10000;
}


# Open and read bed and ref files into hashes
open FDR1_BEDFILE, $fdr1_bedfile or die "Can't open $fdr1_bedfile: $!";
while (my $bedline = <FDR1_BEDFILE>) {
	chomp $bedline;
	my @tmp = split /\t/, $bedline;
	my $bedchr = $tmp[0];
	my $bedstart = $tmp[1];
	my $bedend = $tmp[2];
	my $bedcoord = $bedstart . "\t" . $bedend;
	$fdr1{$bedchr}{$bedcoord}++;
}
close FDR1_BEDFILE;

open FDR10_BEDFILE, $fdr10_bedfile or die "Can't open $fdr10_bedfile: $!";
while (my $bedline = <FDR10_BEDFILE>) {
	chomp $bedline;
	my @tmp = split /\t/, $bedline;
	my $bedchr = $tmp[0];
	my $bedstart = $tmp[1];
	my $bedend = $tmp[2];
	my $bedcoord = $bedstart . "\t" . $bedend;
	$fdr10{$bedchr}{$bedcoord}++;
}
close FDR10_BEDFILE;

open REFFILE, $reffile or die "Can't open $reffile: $!";
readline(REFFILE);
while (my $refline = <REFFILE>) {
	chomp $refline;
	my @tmp = split /\t/, $refline;
	my $refchr = "chr" . $tmp[0];
	my $refstart = $tmp[2];
	my $refend = $tmp[3];
	my $fbgn = $tmp[6];
	my $refcoord = $refstart . "\t" . $refend;
	$genes{$refchr}{$fbgn} = $refcoord;
}
close REFFILE;


# FDR1%: assign overlapping intervals to genes
foreach my $chr (keys %fdr1) {
	foreach my $coord (keys %{$fdr1{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $flag = 0;
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};
			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
			if (($intstart >= $genestart and $intstart <= $geneend) or
				($intend >= $genestart and $intend <= $geneend) or
				($genestart >= $intstart and $genestart <= $intend) or
				($geneend >= $intstart and $geneend <=$intend)) {
				$fdr1_hits{$fbgn}++;
				$flag++;
			}
		}		
		if ($flag == 0) {
			$fdr1_unresolved{$chr}{$coord}++;
		}
	}
}

# FDR1%: assign non-overlapping intervals to genes
foreach my $chr (keys %fdr1_unresolved) {
	foreach my $coord (keys %{$fdr1_unresolved{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $candidate = "none";
		my $dist = $maxdist;
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};
			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
			# Gene is upstream of interval
			if ($geneend < $intstart) {
				my $genedist = $intstart - $geneend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
			# Gene is downstream of interval
			if ($genestart > $intend) {
				my $genedist = $genestart - $intend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
		}
		unless ($candidate eq "none") {
			$fdr1_hits{$candidate}++;
		}
	}
}

# FDR10%: assign overlapping intervals to genes
foreach my $chr (keys %fdr10) {
	foreach my $coord (keys %{$fdr10{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $flag = 0;
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};
			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
			if (($intstart >= $genestart and $intstart <= $geneend) or
				($intend >= $genestart and $intend <= $geneend) or
				($genestart >= $intstart and $genestart <= $intend) or
				($geneend >= $intstart and $geneend <=$intend)) {
				$fdr10_hits{$fbgn}++;
				$flag++;
			}
		}		
		if ($flag == 0) {
			$fdr10_unresolved{$chr}{$coord}++;
		}
	}
}

# FDR10%: assign non-overlapping intervals to genes
foreach my $chr (keys %fdr10_unresolved) {
	foreach my $coord (keys %{$fdr10_unresolved{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $candidate = "none";
		my $dist = $maxdist;
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};
			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
			# Gene is upstream of interval
			if ($geneend < $intstart) {
				my $genedist = $intstart - $geneend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
			# Gene is downstream of interval
			if ($genestart > $intend) {
				my $genedist = $genestart - $intend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
		}
		unless ($candidate eq "none") {
			$fdr10_hits{$candidate}++;
		}
	}
}


# Count number of peaks for each gene
foreach my $fbgn (keys %fdr1_hits) {
	if ($fdr1_hits{$fbgn} >= 2 and $fdr10_hits{$fbgn} >= 5) {
		$multipeaks_genes{$fbgn}++;
	}
	elsif ($fdr1_hits{$fbgn} == 1 and $fdr10_hits{$fbgn} == 1) {
			$singlepeak_genes{$fbgn}++;
	}
	else {
		$unclassified{$fbgn}++;
	}
}


# Create output files and write FBgns
(my $multipeaks_genes_file = $fdr1_bedfile) =~ s/(.+)\.bed/$1_MULTIPEAKS\.txt/;
(my $singlepeak_genes_file = $fdr1_bedfile) =~ s/(.+)\.bed/$1_SINGLEPEAK\.txt/;
{
	open my $FH, ">", $multipeaks_genes_file or die "Can't open $multipeaks_genes_file: $!";
	foreach my $fbgn (keys %multipeaks_genes) {
		print $FH "$fbgn\n";
	}
	close $FH
}
{
	open my $FH, ">", $singlepeak_genes_file or die "Can't open $singlepeak_genes_file: $!";
	foreach my $fbgn (keys %singlepeak_genes) {
		print $FH "$fbgn\n";
	}
	close $FH
}
