#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

# If dealing with a TiMAT gff3 file, please use the gff3_only_intervals script first.

use strict;
use warnings;

unless ($ARGV[0]) {
	print "USAGE: $0 gffFile.gff > bedFile.bed\n";
	exit;
}

my $gffFile = $ARGV[0];

open my $GFFFILE, "<", $gffFile or die "Can't open $gffFile: $!";
while (my $line = <$GFFFILE>) {
	next if $line =~ /^#/;
	my @tmp = split /\t/, $line;
	
	my $chr;
	if ($tmp[0] =~ /^chr/) {
		$chr = $tmp[0];
	}
	else {
		$chr = "chr" . $tmp[0];
	}
	
	my $start = $tmp[3] - 1;
	if ($start <= 0) {
		$start = 1;
	}
	
	my $end = $tmp[4];
	
	if ($end - $start > 0) {
		print $chr . "\t" . $start . "\t" . $end . "\n";
	}
}



