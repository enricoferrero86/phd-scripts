#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 [gs]bedfile.[gs]bed dataset_name > results.R\n";
	exit;
}

my $sbedfile = $ARGV[0];
my $dataset = $ARGV[1];

my %intervals;
my $int_count;
my %windows;
my $win_count;

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line=<$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	
	my $l_limit = $nt - 5000;
	my $r_limit = $nt + 5000;
	my $limits = $l_limit . "\t" . $r_limit;
	
	$intervals{$chr}{$limits}++;
}


my %genome_sequence;
my $PATH2SEQ = "/flychip/share/Genomes/UCSCmirror/dm3/bigZip/";
my @chrom = ("chr2L", "chr2LHet", "chr2R", "chr2RHet", "chr3L", "chr3LHet", "chr3R", "chr3RHet", "chr4", "chrX", "chrXHet", "chrYHet", "chrM", "chrU", "chrUextra");

foreach my $chr (@chrom) {
	open FILE, $PATH2SEQ.$chr.".fa";
	while (defined (my $line = <FILE>)) {
		chomp $line;
		if ($line !~ /\>/) {
			$genome_sequence{$chr} .= uc $line;
	  	}
  	}
	close FILE;
}


foreach my $chr (keys %intervals) {
	foreach my $limits (keys %{$intervals{$chr}}) {
		
		$int_count++;
		
		my @tmp = split /\t/, $limits;
		my $l_limit = $tmp[0];
		my $r_limit = $tmp[1];
		
		my $win_start = $l_limit;
		my $win_end = $win_start + 200;
		$win_count = 0;
	
		while ($win_end <= $r_limit) {
			my $seq = extract_seq ($chr, $win_start, $win_end);
			my $GC_perc;
			if (defined $seq) {
				$GC_perc = count_GC_perc ($seq);
			}
			else {
				$GC_perc = 42.5;
			}
			$windows{$win_count}{$GC_perc}++;
			$win_start = $win_end;
			$win_end = $win_start + 200;
			$win_count++;
		}
	}
}


print "\n$dataset <- c(";
foreach my $win_count (sort {$a <=> $b} keys %windows) {
		my $GC_temp;
		my $GC_sum;
		my $GC_avg;
		foreach my $GC_perc (keys %{$windows{$win_count}}) {
		my $n = $windows{$win_count}{$GC_perc};
		$GC_temp = $n * $GC_perc;
		$GC_sum += $GC_temp;
		}
	$GC_avg = $GC_sum / $int_count;
	print "$GC_avg, ";
}
print ")\n\n";	
		
sub extract_seq {
	my ($chr, $start, $end) = @_;
	my $seq = substr($genome_sequence{$chr}, $start, $end-$start);
	return $seq;
}


sub count_GC_perc {
	my ($seq) = @_;
	
	my $A_count = 0;
	my $C_count = 0;
	my $G_count = 0;
	my $T_count = 0;
	my $N_count = 0;
	
	my $GC_perc;
	
	my @seq = split //, $seq;
	foreach my $nt (@seq) {
		if ($nt eq 'A') {
			$A_count++;
		}		
		if ($nt eq 'C') {
			$C_count++;
		}		
		if ($nt eq 'G') {
			$G_count++;
		}		
		if ($nt eq 'T') {
			$T_count++;
		}
		else {
			$N_count++;
		}
	}
	$GC_perc = (($C_count + $G_count + (0.425 * $N_count)) / ($A_count + $C_count + $G_count + $T_count + $N_count)) * 100;
	return $GC_perc;
}




		
		
