#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 bed_file_1.bed bed_file_2.bed\n";
	exit;
}

my $bedfile1 = $ARGV[0];
my $bedfile2 = $ARGV[1];

(my $name = $bedfile1) =~ s/(.+?)\..+/$1/;



my %int1;
my %new_int;

open BEDFILE1, $bedfile1 or die "Can't open $bedfile1: $!";
while (my $bedline = <BEDFILE1>) {
	chomp $bedline;
	my @tmp = split /\t/, $bedline;
	my $chr=$tmp[0];
	my $ntl=$tmp[1];
	my $ntr=$tmp[2];
	my $score=$tmp[4];
	my $coord = $ntl . "\t" . $ntr;
	$int1{$chr}{$coord} = $score;
}
close BEDFILE1;

open BEDFILE2, $bedfile2 or die "Can't open $bedfile1: $!";
while (my $bedline = <BEDFILE2>) {
	chomp $bedline;
	my @tmp = split /\t/, $bedline;
	my $chr=$tmp[0];
	my $ntl=$tmp[1];
	my $ntr=$tmp[2];
	my $score=$tmp[4];
	
	foreach my $coord (keys %{$int1{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $ntl1 = $tmp[0];
		my $ntr1 = $tmp[1];
		my $score1 = $int1{$chr}{$coord};
		
		if ((($ntl >= $ntl1) && ($ntl <= $ntr1)) || (($ntr >= $ntl1) && ($ntr <= $ntr1)) || (($ntl1 >= $ntl) && ($ntl1 <= $ntr)) || (($ntr1 >= $ntl) && ($ntr1 <= $ntr))) {
			my $new_ntl;
			my $new_ntr;
			if ($ntl < $ntl1) {
				$new_ntl = $ntl1;
			} else {
				$new_ntl = $ntl;
			}
			if ($ntr < $ntr1) {
				$new_ntr = $ntr;
			} else {
				$new_ntr = $ntr1;
			}
			if ($score1 > $score) {
				my $new_coord = $chr . "\t" . $new_ntl . "\t" . $new_ntr . "\t" . $name . "\t" . $score1 . "\n";
				unless ($new_ntl == $new_ntr) { # Fixes a small bug so that results are identical to bedtools intersect
					$new_int{$new_coord}++;
				}
			}
		}
	}

}
close BEDFILE2;


foreach (sort keys %new_int) {
	print "$_";
}
