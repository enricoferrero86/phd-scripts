#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero, Bettina Fischer.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# Original code by Bettina Fischer, modified by Enrico Ferrero.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


# This script is original based on Jelena's interval_overlap_2way and new_fusion
# scripts
# This script compares intervals (peak) *.bed files and counts the number of
# overlapping peaks (exact overlaps!).
# opt_u: write union between the overlapping peaks to _union.bed file
# opt_i: write intersect between overlapping peaks to _intersect.bed file
# add to report the unmatched peaks from each file
# Bettina Fischer, 20101117
# updated 20120710 to implement the minimum number of overlapping bp,
#	also now have to use -a and -b for selecting the bed files
# update to implement a merge options -m, 20120718


use strict;
use Getopt::Std;

our($opt_a, $opt_b, $opt_u, $opt_i, $opt_h, $opt_o, $opt_m);
getopt('abo');

if($opt_h || !$opt_a || !$opt_b) {
    print <<EOF;

  Usage: peak_overlap_bed.pl -a file1.bed -b file2.bed

  Options: 
	-a: specify first bed file

	-b: sepcify second bed file

	-o: specify the minimum number of overlapping bp
				(default 1 bp)

	-u: write union to _union.bed file

	-i: write intersect to _intersect.bed

	-m: merge peaks of both bed files into one (keeping all)


Example:

F1.bed    |-----|        |-------|    |--------|              |---------------|   
F2.bed       |-----|         |-------------|      |-------|   |----------|       |------|


Union     |--------|     |---------------------|              |---------------|

Intersect    |--|            |---|    |----|                  |----------|

Merge     |--------|     |---------------------|  |--------|  |---------------|  |------|


Peaks in F1.bed: 4
Peaks in F2.bed: 5
Unique peaks in F1.bed: 0
Unique peaks in F2.bed: 2
Union peaks: 3
Intersect peaks: 4
Merged peaks: 5
  

EOF
exit;
}


my $file1;						# first bed file - opt_a
my $file2;						# second bed file  - opt_b

my @TF1;
my @TF2;
my $file1_peak_cnt = 0;        # count peaks of file1
my $file2_peak_cnt = 0;        # count peaks of file2
my ($ntl, $ntr, $ntl2, $ntr2); # start and end pf peaks for file 1 & 2
my ($chr, $chr2);
my $newvar;

#my $tf1_counter = 0;           # counts overlaps file1 vs file2
#my $tf2_counter = 0;           # counts overlaps file2 vs file1
my $union_counter = 0;         # counts union peaks of overlaps
my $intersect_counter = 0;     # counts intersect peaks of overlaps

my %union;
my @union;
my $union;
my @intersect;
my $intersect;
my $unionfile;
my $intersectfile;
my $mergefile;
my %seen;                      # for union ensure that peak is only added once
my %seen2;                     # for intersect ensure peak added only once

my @matched_TF1;               # to be able to count the matched peaks and subract from the total
my @matched_TF2;
my %seenmatch1;
my %seenmatch2;

my $min_overlap = 1;			# minimum bp of overlap

if($opt_a =~ m/\S+\.bed/) {
	$file1 = $opt_a;
} else {
	die "Please sepecify the first *.bed file with -a!\n";
}
if($opt_b =~ m/\S+\.bed/) {
	$file2 = $opt_b;
} else {
	die "Please sepecify the second *.bed file with -b!\n";
}

# get the minimum overlap, otherwise use the default of 1bp
if($opt_o =~ m/\d+/) {
	$min_overlap = $opt_o;
}
print "Using minumum overlap distance of: $min_overlap bp\n";


# specify the result file name(s)
if($opt_u || $opt_i || $opt_m) {
    my $name1;
    my $name2;
    if ($file1 =~ m/(\S+)\.bed$/) {
	$name1 = $1;
    }
    if ($file2 =~ m/(\S+)\.bed$/) {
	$name2 = $1;
    }
    if($opt_u) {
	$unionfile = "$name1"."_"."$name2"."_m"."$min_overlap"."_union.bed";
    }
    if($opt_i) {
	$intersectfile = "$name1"."_"."$name2"."_m"."$min_overlap"."_intersect.bed";
    }
	if($opt_m) {
	$mergefile = "$name1"."_"."$name2"."_merge.bed";
	}
}


# read peaks of file1 into array
my $line = "";
open (FILE, "$file1") or die "Cannot open $file1: !$\n";
while ($line = <FILE>) {
    s/HET/Het/ig;
    chomp $line;
    my @tmp = split(/\t/, $line);
    $chr = $tmp[0];
    
    # need to remove leading zeros in TAMAL files to be able to compare to TiMAT files
    if ($tmp[1] =~ m/^0*([1-9]\d+)/) {
	$ntl = $1;
    }
    if ($tmp[2] =~ m/^0*([1-9]\d+)/) {
	$ntr = $1;
    }
    
    $file1_peak_cnt++;

    $newvar = "$chr\t$ntl\t$ntr";
    push(@TF1, $newvar);

}
close FILE;


# read peaks of file2 into array
$line = "";
open (FILE1, "$file2") or die "Cannot open $file2: !$\n";
while ($line = <FILE1>) {
    s/HET/Het/ig;
    chomp $line;
    my @tmp = split(/\t/, $line);
    $chr = $tmp[0];

    # need to remove leading zeros in TAMAL files to be able to compare to TiMAT files
    if ($tmp[1] =~ m/^0*([1-9]\d+)/) {
	$ntl = $1;
    }
    if ($tmp[2] =~ m/^0*([1-9]\d+)/) {
	$ntr = $1;
    }
    
    $file2_peak_cnt++;

    $newvar = "$chr\t$ntl\t$ntr";
    push(@TF2, $newvar);

}
close FILE1;

print "Peaks in $file1: $file1_peak_cnt\n";
print "Peaks in $file2: $file2_peak_cnt\n";


foreach my $tmp (@TF1) {
    my @tmp = split(/\t/, $tmp);
    $chr = $tmp[0];
    $ntl = $tmp[1];
    $ntr = $tmp[2];

    foreach my $tmp2 (@TF2) {
	my @tmp2 = split(/\t/, $tmp2);
	$chr2 = $tmp2[0];
	$ntl2 = $tmp2[1];
	$ntr2 = $tmp2[2];

	if($chr eq $chr2) {

	    # count if the start is exactly the same (only need to do this once)
	    if ($ntl2 == $ntl) { 
			if($ntr2 > $ntr) {
				if(($ntr - $ntl) >= $min_overlap) {		# ok
					$union = "$chr\t$ntl\t$ntr2";
				}
			} else {
				if(($ntr2 - $ntl) >= $min_overlap) {	# ok
					$union = "$chr\t$ntl\t$ntr";
				}
			}
			push (@union, $union) unless $seen{$union}++;
			
			if($ntr2 < $ntr) {
					$intersect = "$chr\t$ntl\t$ntr2";
			} else {
					$intersect = "$chr\t$ntl\t$ntr";
			}
			push (@intersect, $intersect) unless $seen2{$intersect}++;

			
			push (@matched_TF1, $tmp) unless $seenmatch1{$tmp}++;
			push (@matched_TF2, $tmp2) unless $seenmatch2{$tmp2}++;

	    }

	    # count if start TF2 > start TF1
	    if (($ntl2 > $ntl) && ($ntl2 < $ntr)) {
			if($ntr2 > $ntr) {
				if(($ntr - $ntl2) >= $min_overlap) {		# ok
					$union = "$chr\t$ntl\t$ntr2";
				}
			} else {
				if(($ntr2 - $ntl2) >= $min_overlap) {		# ok
					$union = "$chr\t$ntl\t$ntr";
				}
			}
			push (@union, $union) unless $seen{$union}++;
			 
			if($ntr2 < $ntr) {
				$intersect = "$chr\t$ntl2\t$ntr2";
			} else {
					$intersect = "$chr\t$ntl2\t$ntr";
			}
			push (@intersect, $intersect) unless $seen2{$intersect}++;

			push (@matched_TF1, $tmp) unless $seenmatch1{$tmp}++;
			push (@matched_TF2, $tmp2) unless $seenmatch2{$tmp2}++;

	    }

	    # count if start TF2 < start TF1
	    if (($ntl > $ntl2) && ($ntl < $ntr2)) {
			if($ntr2 > $ntr) {
				if(($ntr - $ntl)  >= $min_overlap) {		# ok
					$union = "$chr\t$ntl2\t$ntr2";
				}
			} else {
				if(($ntr2 - $ntl)  >= $min_overlap) {		# ok
					$union = "$chr\t$ntl2\t$ntr";
				}
			}
			push (@union, $union) unless $seen{$union}++;
			 
			if($ntr2 < $ntr) {
					$intersect = "$chr\t$ntl\t$ntr2";
			} else {
					$intersect = "$chr\t$ntl\t$ntr";
			}
			push (@intersect, $intersect) unless $seen2{$intersect}++;

			push (@matched_TF1, $tmp) unless $seenmatch1{$tmp}++;
			push (@matched_TF2, $tmp2) unless $seenmatch2{$tmp2}++;
	    }		
	}
    }	
}	

foreach my $tmp (@TF2) {
    my @tmp = split(/\t/, $tmp);
    $chr = $tmp[0];
    $ntl = $tmp[1];
    $ntr = $tmp[2];

    foreach my $tmp2 (@TF1) {
	my @tmp2 = split(/\t/, $tmp2);
	$chr2 = $tmp2[0];
	$ntl2 = $tmp2[1];
	$ntr2 = $tmp2[2];

	if($chr eq $chr2) {

	    # count if start TF2 > start TF1
	    if (($ntl2 > $ntl) && ($ntl2 < $ntr)) {
			if($ntr2 > $ntr) {
				if(($ntr - $ntl2)  >= $min_overlap) {	# ok
					$union = "$chr\t$ntl\t$ntr2";
				}
			} else {
				if(($ntr2 - $ntl2)  >= $min_overlap) {	# ok
					$union = "$chr\t$ntl\t$ntr";
				}
			}
			push (@union, $union) unless $seen{$union}++;
			 
			if($ntr2 < $ntr) {
					$intersect = "$chr\t$ntl2\t$ntr2";
			} else {
					$intersect = "$chr\t$ntl2\t$ntr";
			}
			push (@intersect, $intersect) unless $seen2{$intersect}++;

			push (@matched_TF2, $tmp) unless $seenmatch2{$tmp}++;
			push (@matched_TF1, $tmp2) unless $seenmatch1{$tmp2}++;
	    }

	    # count if start TF2 < start TF1
	    if (($ntl > $ntl2) && ($ntl < $ntr2)) {
			if($ntr2 > $ntr) {
				if(($ntr - $ntl)  >= $min_overlap) {	# ok
					$union = "$chr\t$ntl2\t$ntr2";
				}
			} else {
				if(($ntr2 - $ntl)  >= $min_overlap) {	# ok
					$union = "$chr2\t$ntl2\t$ntr";
				}
			}
			push (@union, $union) unless $seen{$union}++;
			 
			if($ntr2 < $ntr) {
					$intersect = "$chr\t$ntl\t$ntr2";
			} else {
					$intersect = "$chr\t$ntl\t$ntr";
			}
			push (@intersect, $intersect) unless $seen2{$intersect}++;

			push (@matched_TF2, $tmp) unless $seenmatch2{$tmp}++;
			push (@matched_TF1, $tmp2) unless $seenmatch1{$tmp2}++;

	    }		
	}
    }	
}	


my $match_TF1_cnt = @matched_TF1;
my $match_TF2_cnt = @matched_TF2;
#print "Matched peaks in $file1: $match_TF1_cnt\n";
#print "Matched peaks in $file2: $match_TF2_cnt\n";
my $unique_TF1_cnt = $file1_peak_cnt - $match_TF1_cnt;
my $unique_TF2_cnt = $file2_peak_cnt - $match_TF2_cnt;

print "Unique peaks in $file1: $unique_TF1_cnt\n";
print "Unique peaks in $file2: $unique_TF2_cnt\n";


# If 2 peaks from one file and one peak from the other overlap it is present
# twice, so need to get the full union region into one new union peak

foreach my $line (@union) {
    my @tmp = split(/\t/, $line);
    my $chr = $tmp[0];
    my $ntl = $tmp[1];
    my $ntr = $tmp[2];
    foreach my $number ($ntl .. $ntr) {
	$union{$chr}{$number}++;
    }
}


if ($opt_u) {
    open (UNION, ">$unionfile");
    print "Writing union of overlapping peaks to: $unionfile\n";
}

foreach my $c (keys %union) {
	
    #find out maximum
    my $max = 0;
    foreach my $n (keys %{$union{$c}}) {
	if ($n > $max) {
	    $max = $n;
	}
    }
	
    #go through and look for intervals
    my $count = 0;
    while ($count < $max) {
	$count++;
	if (exists ($union{$c}{$count})) {
	    if ($opt_u) {
		print UNION "$c\t$count\t";
	    }
	    my $flag = 0;
	    
	    while ($flag < 1) {
		my $new_count = $count + 1;
		if (exists ($union{$c}{$new_count})) {
		    $count++;
		} else {
		    if ($opt_u) {
			print UNION "$count\n";
		    }
		    $flag = 1;
		    $union_counter++;
		}
	    }
	}
    }	
}
close UNION;


if ($opt_i) {
    print "Writing intersect of overlapping peaks to: $intersectfile\n";
    open (INTERSECT, ">$intersectfile") or die "Cannot open $intersectfile: $!\n";
    foreach my $tmp (@intersect) {
	print INTERSECT"$tmp\n";
	$intersect_counter++;
    }
    close INTERSECT;
}

if(!$opt_i) {
    foreach my $tmp (@intersect) {
	$intersect_counter++;
    }
}

print "Union peaks: $union_counter\n";
print "Intersect peaks: $intersect_counter\n";


### implement merge 2 bed files keeping all peaks
if($opt_m) {
	my %int;
	my @bedFiles = ($file1, $file2);
	my $merge_cnt = 0;

	foreach my $file (@bedFiles) {
		open my $FILE, "<", $file or die "Can't open $file: $!";
		while (my $line = <$FILE>) {
			my @tmp = split /\t/, $line;
			my $chr = $tmp[0];
			my $ntl = $tmp[1];
			my $ntr = $tmp[2];
			foreach my $nt ($ntl .. $ntr) {
				$int{$chr}{$nt}++;
			}
		}
	}

	open(MERGE, ">$mergefile") or die "Cannot open $mergefile!\n";
	print "Merging peaks to: $mergefile\n";

	foreach my $chr (keys %int) {
		my $max = 0;
		foreach my $nt (keys %{$int{$chr}}) {
			if ($nt > $max) {
				$max = $nt;
			}
		}
		my $flag = 0;
		foreach my $nt (0 .. $max+1) {
			if ($flag == 0) {
				if (exists ($int{$chr}{$nt})) {
					my $ntl = $nt;
					print MERGE "$chr\t$ntl\t";
					$flag = 1;
					$merge_cnt++;
				}
			}
			if ($flag == 1) {
				if (exists ($int{$chr}{$nt})) {
				}
				else {
					my $ntr = $nt-1;
					print MERGE "$ntr\n";
					$flag = 0;
					$merge_cnt++;
				}
			}
		}
	}
	close MERGE;
	print "Merged peaks: $merge_cnt\n\n";

}

print "\n";
