#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %peaks;
my %genes;

my $genic = 0;
my $intergenic = 0;
my $total = 0;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 sbed_file.sbed flymine_ref_file.ref > genomic_features.txt\n";
	exit;
}

my $sbedfile = $ARGV[0];
my $reffile = $ARGV[1];

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $maxnt = $tmp[3];
	$peaks{$chr}{$maxnt} = 1;
	$total++;
}
close $SBEDFILE;

open my $REFFILE, "<", $reffile or die "Can't open $reffile: $!";
readline $REFFILE;
while (my $line = <$REFFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = "chr" . $tmp[0];
	my $start = $tmp[2];
	my $end = $tmp[3];
	foreach my $nt ($start .. $end) {
		$genes{$chr}{$nt} = 1;
	}
}
close $REFFILE;

foreach my $chr (keys %peaks) {
	foreach my $nt (keys %{$peaks{$chr}}) {
		if (exists $genes{$chr}{$nt}) {
			$genic++;
		}
		else {
			$intergenic++
		}
	}
}

unless (($genic + $intergenic) == $total) {
	print "Something is wrong!\n";
	exit;
}

my $genic_perc = ($genic / $total) * 100;
my $intergenic_perc = ($intergenic / $total) * 100;

printf "Genomic features for $sbedfile ($total):\ngenic: %.2f%% ($genic)\nintergenic: %.2f%% ($intergenic)\n", $genic_perc, $intergenic_perc;
	
	
