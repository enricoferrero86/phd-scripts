#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless (@ARGV) {
	print "USAGE: k-means_file.csv > results.R";
	exit;
}

my $file = $ARGV[0];
my $data = $ARGV[1];

my %groups;

open FH, "<", $file or die;
readline(FH);
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $gene = $tmp[0];
	my $group = $tmp[1]+1;
	my $values = $tmp[2].",".$tmp[3].",".$tmp[4].",".$tmp[5].",".$tmp[6];
	$groups{$group}{$gene} = $values;
}
close FH;

foreach my $group (keys %groups) {
	foreach my $gene (keys %{$groups{$group}}) {
		my $values = $groups{$group}{$gene};
		print "lines(c($values),col=$group)\n";
	}
	print "\n\n";
}

	
	
