#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;


# Parameters:
my $pvalueCutoff = 0.05; # P-Value cutoff, it can be 0.05, 0.01, 0.001
my $numberOfTerms = 15; # Only consider top numberOfTerms for each dataset, set to a high value (10000) to consider all terms.

unless (@ARGV) {
	print "USAGE: $0 BiNGO_cluster_1.bgo BiNGO_cluster_2 ... > GO_table.txt\n";
	print "Multiple files can be used as input files\n";
	exit;
}

my %sig_terms;
my %datasets;

foreach my $file (@ARGV) {
	(my $dataset = $file) =~ s/(.+)\.bgo/$1/;
	$datasets{$dataset}++;
	open my $FILE, "<", $file or die "Can't open $file: $!";
	my $counter = 1;
	my $flag = 0;
	while (my $line = <$FILE>) {
		if ($line =~ /^GO-ID/) {
			$flag = 1;
		}
		if ($flag == 1) {
			if ($counter <= $numberOfTerms) {
				if ($line =~ /^\d+/) {
					$counter++;
					my @tmp = split /\t/, $line;
					my $go_name = $tmp[7];
					my $pvalue = $tmp[2];
					if ($pvalue <= $pvalueCutoff)  {
						$sig_terms{$go_name}{$dataset} = $pvalue;
					}
				}
			}
		}
	}
}

print "GO Term\t";
foreach my $dataset (sort keys %datasets) {
	print "$dataset\t";
}
print "\n";	


foreach my $go_name (keys %sig_terms) {
		print "$go_name\t";
		foreach my $dataset (sort keys %datasets) {
			my $pvalue = 1;
			if (exists $sig_terms{$go_name}{$dataset}) {
				$pvalue = $sig_terms{$go_name}{$dataset};
			}
			print "$pvalue\t";
		}
		print "\n";
}
