#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero, Jelena Aleksic.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# Original code by Jelena Aleksic, modified by Enrico Ferrero
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

#finds the distance from nearest transcription start site up to 1kb upstream or downstream
#input - peak centre coordinates

use strict;
use DBI;

sub run_query {
  my $SQL_statement = $_[0];
  $_[2] = $_[1]->prepare($SQL_statement);
  $_[2]->execute() or die("fucking hell, your SQL was wrong");
}

my $database_handle1 = DBI->connect("DBI:mysql:enrico:127.0.0.1:7777","enrico","enricoSQL");
my $st;
my $st2;

my $data = $ARGV[1];


my $RANGE = 5000;
my $totalcount = 0;
my $nogenescount = 0;

print "$data <- c(";

open FILE, $ARGV[0];
my $line = "";
while ($line = <FILE>) {
	chomp $line;
	my @tmp = split (/\t/, $line);
	my $chr = $tmp[0];
	my $nt = $tmp[3];

	my $ntl = $nt - $RANGE;
	my $ntr = $nt + $RANGE;
	
	my $distance = "";
	
	my $counter = 0;
	
	run_query qq(SELECT strand, txStart FROM UCSC_dm3.flyBaseGene WHERE chrom="$chr" AND (txStart BETWEEN $ntl AND $ntr) GROUP BY (txStart)), $database_handle1, $st2;	
			while (my ($strand, $txstart) = $st2->fetchrow_array) {
				my $curr_distance = "";
				if ($txstart > $nt) {
					$distance = $txstart - $nt;
					$curr_distance = $distance;
				} else {
					$distance = $nt - $txstart;
					$curr_distance = -$distance;
				}
				
				if ($curr_distance < $RANGE) {
					if ($strand eq '-') {
						$curr_distance = -$curr_distance;
					}
					print "$curr_distance,";
				}
			}
}
	
close FILE;

print ");\n\n";

#print "total coords: $totalcount\nNo genes hit within $RANGE bp: $nogenescount\n";			
