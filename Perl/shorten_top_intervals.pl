#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %intervals;
my $number;
my $length;

unless ($ARGV[0]) {
	print "USAGE: $0 [gs]bed_file.[gs]bed [number of intervals] [legth of interval]\n";
	print "INFO: Default values are 100 intervals 300 bp long\n";
	exit;
}
my $sbedfile = $ARGV[0];

if ($ARGV[1] and $ARGV[2]) {
	if (($ARGV[1] % 2 != 0) or ($ARGV[2] %2 != 0)) {
		print "Please use even numbers for both 'number of intervals' and 'length of interval'\n";
		exit;
	}
	$number = $ARGV[1];
	$length = $ARGV[2];
}
else {
	$number = 100;
	$length = 300;
}

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	my $dist = $end - $start;
	if ($dist > $length) {
		my $midpoint = $nt;
		$start = $midpoint - ($length / 2);
		$end = $midpoint + ($length / 2);
	}
	my $coord = $start . "\t" . $end;
	$intervals{$score}{$chr}{$coord} = 1;
}
close $SBEDFILE;

my @scores = sort {$b <=> $a} keys %intervals;
my @topscores = @scores[0 .. $number-1];

(my $bedfile = $sbedfile) =~ s/(.+)\..+/$1\_n$number\_l$length\.bed/;
open my $BEDFILE, ">", $bedfile or die "Can't open $bedfile; $!";
foreach my $score (@topscores) {
	foreach my $chr (keys %{$intervals{$score}}) {
		foreach my $coord (keys %{$intervals{$score}{$chr}}) {
			print $BEDFILE $chr . "\t" . $coord . "\n";
		}
	}
}
close $BEDFILE;



