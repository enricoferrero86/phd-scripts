#! /usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero, Bettina Fischer.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# Original code by Bettina Fischer, modified by Enrico Ferrero.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


# map bed file peaks within directory to closest TSS or gene (FBgn) using
# the latest gene positions created by the FL003 annotation mapping
# The script creates *_closestTSSorGeneList.txt and 
# *_closestTSSorGeneTable.txt files.
# Bettina Fischer, 20120807
# 20120905, fixed NA count bug
# 20121128, implemented FlyBase release selection and add d to filename
# 20130529, some edits by Enrico Ferrero: 
# 	- operate only on BED files passed as arguments, not all BED files in current directory;
#	- print "working on *.bed instead that *_geneTable.txt; 
#	- do not output the gene table;
#	- output a list of FBgns and a list of GeneSymbols, with no header.

use strict;
use Getopt::Std;

our($opt_h, $opt_d, $opt_r);
getopt('dr');

my $dir = `pwd`;
chomp $dir;
my $MAX_DIST = 10000;			# max distance to gene
my @bedFiles = @ARGV;
my $release;					# optional specified with -r option


if($opt_h) {
	print <<EOF;

===============================================================================

	Usage: bed2closestTSSorGene.pl bedfile_1.bed [bedfile_2.bed ... bedfile_n.bed] 

	Options:
		-h: help

		-d: specify max distance to next TSS / gene in bp - default 10 kb

		-r: specify FlyBase release verison, e.g. -r 5.46 
			(available from version r5.46 upwards)

	This script takes bed files passed as arguments and finds 
	the closest Transcription Start Site TSS of gene(s) withing the specified 
	max distance. If a peak cannot be assigned to a TSS within the max distance
	distance to gene borders are considered.

	Gene TSS and borders are calculated using the furthest boundaries for all
	transcripts currently annotated.


	The *.FBgns.txt contains the unique FBgns.
	The *.GeneSymbols.txt contains the unique GeneSymbols.


	Note: If a peak hits inside more than one gene all of these genes are 
		reported. If this gene is very large the distance to the gene boundaries
		can exceed the 10hb distance.

===============================================================================

EOF
exit;
}


### get the distance from opt_d
if($opt_d =~ m/(\d+)/) {
	$MAX_DIST = $1;
}

## get the FlyBase release version to be used for annotation
if($opt_r =~ m/\d+\.\d+/) {
	$release = "r" . $opt_r;
}


### get the latest annotation folder and annotation file name
chdir "/flychip/production/FL003_transcript_mapping/";
my @annotationFolders = glob "FlyBase_*";
my @sortedFolders = sort { $b cmp $a }@annotationFolders;
my $annotationFolder = $sortedFolders[0];
if($opt_r) {
	$annotationFolder = "FlyBase_"."$release";
}
chdir "/flychip/production/FL003_transcript_mapping/$annotationFolder/";

my $annotationFile = "GenePosTable.txt";
my $fullAnnFilePath = "/flychip/production/FL003_transcript_mapping/$annotationFolder/$annotationFile";
chdir "$dir";

print "\nUsing gene annotaion version:\t$annotationFolder\n\n";
print "Selected maximum distance to gene: $MAX_DIST bp\n";

### read all Drosophila gene coordinates, and store it in a hash

open (FILE, "$fullAnnFilePath") or die "Cannot open $fullAnnFilePath!\n\n";

my %table;
my %GeneName;

while (<FILE>) {
	s/\r\n/\n/g;
	s/\r/\n/g;
	s/\"//ig;
	chomp $_;
	if($_ =~ m/FlyBase_release/) { next }

	my @tmp = split(/\t/, $_);
	my $fbgn = $tmp[0];
	my $gene = $tmp[1];
	my $chr = $tmp[2];
	my $strand = $tmp[3];
	my $TSS = $tmp[4];
	my $start = $tmp[5];
	my $end = $tmp[6];

	$table{$chr}{$fbgn} = "$_";
	$GeneName{$fbgn} = "$gene";
}
	
close FILE;

### read each bed file and assign genes to peaks

foreach my $bed (@bedFiles) {

	my %BED = ();
	my $peak_cnt = 0;
	my $not_match_cnt = 0;
	my %NoTSS = ();		# if peak is not close to TSS store in this hash
	my $resultname;
	my $FBgn_listname;
	my $GeneSymbol_listname;
	my @results;
	my @FBgns;
	my %seenFBgn;

	if ($bed =~ /(.+)\.bed/) {
		my $name = $1;
		$resultname = "$name"."_closestTSSorGeneTable_d"."$MAX_DIST".".txt";
		$FBgn_listname = "$name".".FBgns".".txt";
		$GeneSymbol_listname = "$name".".GeneSymbols".".txt";
	}
	print "\nWorking on: $bed\n";

	open(BED, "$bed") or die "Cannot open $bed!\n";

	while (<BED>) {
		s/\r\n/\n/g;
		s/\r/\n/g;
		s/\"//ig;
		chomp $_;
		my @tmp = split(/\t/, $_);
		my $chr = $tmp[0];
		my $ntl = $tmp[1];
		my $ntr = $tmp[2];
		$peak_cnt++;
		my $coord = "$ntl"."\t"."$ntr"."\t"."$peak_cnt";
		#print "$chr\t$ntl\t$ntr\n";
		$BED{$chr}{$coord}++;
	}
	close BED;


	### iterate over the peaks
	foreach my $chr (keys %BED) {
		foreach my $temp (keys %{$BED{$chr}}) {

			my @tmp = split(/\t/, $temp);
			my $pleft = "$tmp[0]";
			my $pright = "$tmp[1]";
			my $peak = "$tmp[2]";
			#print "BED\t$chr\t$pleft\t$pright\t$peak\n";

			my $TSS_min = $MAX_DIST;

			my %FBgn = ();
			my %seen = ();
			my @dist = ();
			my $match_flag = 0;

			foreach my $fbgn (keys %{$table{$chr}}) {
				my @table = split(/\t/, "$table{$chr}{$fbgn}");
				my $TSS = $table[4];
				my $gstart = $table[5];
				my $gend = $table[6];
				my $strand = $table[3];
			
				# find closest Transcription Start Site

				### peak is across TSS - use the shortest distance to peak border
				if(($strand eq "+") && ($pleft <= $TSS && $pright >= $TSS)) {

					# find the closest distance to gene borders
					my $dist_1 = $pleft - $TSS;
					my $dist_2 = $pright - $TSS;
					my @pos = ($dist_1, $dist_2);
					my @sorted_pos = sort { abs($a) <=> abs($b) } (@pos);
					my $dist = $sorted_pos[0];
					$TSS_min = abs($dist);
					#print "@sorted_pos\n";

					#print "$chr\t$pleft\t$pright\t$peak\t$strand\t$fbgn\t$gstart\t$gend\t$TSS\toverlapTSS\t$dist\tNA\n";
					$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapTSS\t$dist\tNA";
					my $tmp = "$fbgn $dist";
					push(@dist, "$dist") unless $seen{$tmp}++;
					$match_flag = 1;	
				}

				if(($strand eq "-") && ($pleft <= $TSS) && ($pright >= $TSS)) {
					
					# find the closest distance to gene borders
					my $dist_1 = $TSS - $pleft;
					my $dist_2 = $TSS - $pright;
					my @pos = ($dist_1, $dist_2);
					my @sorted_pos = sort { abs($a) <=> abs($b) } (@pos);
					my $dist = $sorted_pos[0];
					$TSS_min = abs($dist);
					#print "@sorted_pos\n";

					#print "$chr\t$pleft\t$pright\t$peak\t$strand\t$fbgn\t$gstart\t$gend\t$TSS\toverlapTSS\t$dist\tNA\n";
					$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapTSS\t$dist\tNA";
					my $tmp = "$fbgn $dist";
					push(@dist, "$dist") unless $seen{$tmp}++;
					$match_flag = 1;	
				}


				### peak is upstream to TSS
				if(($strand eq "+") && ($pright <= $TSS)) {
					my $dist = $pright - $TSS;
					if (abs($dist) < $TSS_min) {
						#print "BED\t$strand\t$chr\t$pleft\t$pright\tTSS $TSS\tDistance: $dist\t$fbgn\n";
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tupstream\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				if(($strand eq "-") && ($TSS <= $pleft)) {
					my $dist = $TSS - $pright;
					if (abs($dist) < $TSS_min) {
						#print "BED\t$strand\t$chr\t$pleft\t$pright\tTSS $TSS\tDistance: $dist\t$fbgn\n";
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tupstream\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}


				### peak is inside gene
				if(($strand eq "+") && (($pleft >= $TSS) &&($pright <= $gend))) {
					my $dist = $pleft - $TSS;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tinside\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				if(($strand eq "-") && (($pright <= $TSS) &&($pleft >= $gstart))) {
					my $dist = $TSS - $pright;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tinside\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}


				### peak is across Gene end
				if(($strand eq "+") && (($pleft <= $gend) && ($pright >= $gend))) {
					my $dist = $pleft - $TSS;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapGeneEnd\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				if(($strand eq "-") && (($pleft <= $gstart) && ($pright >= $gstart))) {
					my $dist = $TSS - $pright;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapGeneEnd\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}


				### peak is downstream
				if(($strand eq "+") && ($pleft > $gend)) {
					my $dist = $pleft - $TSS;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tdownstream\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				if(($strand eq "-") && ($pright < $gstart)) {
					my $dist = $TSS - $pright;
					if (abs($dist) < $TSS_min) {
						$TSS_min = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tdownstream\t$dist\tNA";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}
			}

			if($match_flag == 1) {
				# must sort by the absolute value so that shortest distance are
				# the first 2 elements!!!
				my @sorted = sort { abs($a) <=> abs($b) } (@dist);

				#foreach my $dist (@sorted) {
				#	print "$c_tf\t$ntl_tf\t$ntr_tf\t$FBgn{$dist}\t$dist\n";
				#}
				#print "@sorted\n";

				my $result = "$chr\t$pleft\t$pright\t$peak\t$FBgn{$sorted[0]}";
				push(@results, "$result");
			}

			### keep peaks which don't match within distance to a TSS
			if($match_flag == 0) {
				#print "FINAL\t$chr\t$pleft\t$pright\tno match\n";
				my $coord = "$pleft"."\t"."$pright"."\t"."$peak";
				$NoTSS{$chr}{$coord}++;
			}
		}
	}



	### now find the closest genes for the peaks which didn't match to a TSS
	foreach my $chr (keys %NoTSS) {
		foreach my $temp (keys %{$NoTSS{$chr}}) {

			my @tmp = split(/\t/, $temp);
			my $pleft = "$tmp[0]";
			my $pright = "$tmp[1]";
			my $peak = "$tmp[2]";
			#print "BED\t$chr\t$pleft\t$pright\t$peak\n";

			my %FBgn = ();
			my %seen = ();
			my @dist = (),
			my $match_flag = 0;
			my $inside_gene_flag = 0;

			my $GENE_DIST = $MAX_DIST;

			foreach my $fbgn (keys %{$table{$chr}}) {
				my @table = split(/\t/, "$table{$chr}{$fbgn}");
				my $TSS = $table[4];
				my $gstart = $table[5];
				my $gend = $table[6];
				my $strand = $table[3];

				#find clostest gene location (only if no TSS site is within distance)
				
				### peak within a gene
				if((($strand eq "+") &&($pleft >= $TSS) &&($pright <= $gend)) || (($strand eq "-") && ($pright <= $TSS) &&($pleft >= $gstart))) {
					# find the closest distance to gene borders
					my $dist_1 = abs($pleft - $gstart);
					my $dist_2 = abs($pleft - $gend);
					my $dist_3 = abs($pright - $gstart);
					my $dist_4 = abs($pright - $gend);

					my @pos = ($dist_1, $dist_2, $dist_3, $dist_4);
					my @sorted_pos = sort { abs($a) <=> abs($b) } (@pos);

					my $result = "$chr\t$pleft\t$pright\t$peak\t$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tinside\tNA\t$sorted_pos[0]";
					push(@results, "$result");

					$inside_gene_flag = 1;
				}


				### peak is across Gene end
				if(($strand eq "+") && ($pleft <= $gend) && ($pright >= $gend)) {
				
					# find the closest distance to gene borders
					my $dist_1 = abs($pleft - $gstart);
					my $dist_2 = abs($pleft - $gend);
					my $dist_3 = abs($pright - $gstart);
					my $dist_4 = abs($pright - $gend);

					my @pos = ($dist_1, $dist_2, $dist_3, $dist_4);
					my @sorted_pos = sort { abs($a) <=> abs($b) } (@pos);
				
					my $result = "$chr\t$pleft\t$pright\t$peak\t$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapGeneEnd\tNA\t$sorted_pos[0]";
					push(@results, "$result");

					$inside_gene_flag = 1;
				}
				
				if(($strand eq "-") && (($pleft <= $gstart) && ($pright >= $gstart))) {
					
					# find the closest distance to gene borders
					my $dist_1 = abs($pleft - $gstart);
					my $dist_2 = abs($pleft - $gend);
					my $dist_3 = abs($pright - $gstart);
					my $dist_4 = abs($pright - $gend);

					my @pos = ($dist_1, $dist_2, $dist_3, $dist_4);
					my @sorted_pos = sort { abs($a) <=> abs($b) } (@pos);
				
					my $result = "$chr\t$pleft\t$pright\t$peak\t$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\toverlapGeneEnd\tNA\t$sorted_pos[0]";
					push(@results, "$result");

					$inside_gene_flag = 1;
				}
				
				
				# gene is upstream (negative distance)
				if(($strand eq "+") && ($pright <= $gstart)) {
					my $dist = $pright - $gstart;
					if (abs($dist) < $GENE_DIST) {
						$GENE_DIST = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tupstream\tNA\t$dist";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}
					
				# gene is downstream
				if(($strand eq "+") && ($pleft >= $gend)) {
					my $dist = $pleft - $gend;
					if (abs($dist) < $GENE_DIST) {
						$GENE_DIST = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tdownstream\tNA\t$dist";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				# gene is upstream (negative distance)
				if(($strand eq "-") && ($pleft >= $gend)) {
					my $dist = $gend - $pleft;
					if (abs($dist) < $GENE_DIST) {
						$GENE_DIST = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tupstream\tNA\t$dist";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}

				# gene is downstream
				if(($strand eq "-") && ($pright <= $gstart)) {
					my $dist = $gstart - $pright;
					if (abs($dist) < $GENE_DIST) {
						$GENE_DIST = abs($dist);
						$FBgn{$dist} = "$strand\t$fbgn\t$GeneName{$fbgn}\t$gstart\t$gend\t$TSS\tdownstream\tNA\t$dist";
						my $tmp = "$fbgn $dist";
						push(@dist, "$dist") unless $seen{$tmp}++;
						$match_flag = 1;
					}
				}
			}

			if($match_flag == 1) {
				# check if there are 2 genes in list, sort by absolute value
				my @sorted = sort { abs($a) <=> abs($b) } (@dist);

				my $result = "$chr\t$pleft\t$pright\t$peak\t$FBgn{$sorted[0]}";
				push(@results, "$result");

			}

			# also report peaks which didn't match anything
			if(($match_flag == 0) && ($inside_gene_flag == 0)) {
				my $result = "$chr\t$pleft\t$pright\t$peak\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA";
				push(@results, "$result");

				$not_match_cnt++;
			}
		}
	}

	# now get the unique FBgns for the geneList file
	foreach my $result (@results) {
		# now get the unique FBgns for the geneList file
		my @strings = split(/\t/, "$result");
		if($strings[5] =~ m/NA/) { next }
		push(@FBgns, "$strings[5]") unless $seenFBgn{$strings[5]}++;		
	}
	my $unique_cnt = scalar @FBgns;

#~ Uncomment this section to print Bettina's original geneTable
	#~ ### print the geneTable
	#~ open(OUT, ">$resultname") or die "Cannot open $resultname!\n";
	#~ print OUT "\# Gene annotaion version: $annotationFolder\n";
	#~ print OUT "\# Selected maximum distance: $MAX_DIST bp\n";
	#~ print OUT "\# Total peaks: $peak_cnt\n";
	#~ print OUT "\# Unique FBgns: $unique_cnt\n";
	#~ print OUT "\# Not matched: $not_match_cnt\n";
	#~ print OUT "chr\tstart\tend\tpeak\tstrand\tFBgn\tGeneName\tgene_start\tgene_end\tTSS\trelativeToGene\tdistanceToTSS\tshortestDistanceToGene\n";
#~ 
	#~ foreach my $result (@results) {
		#~ print OUT "$result\n";
	#~ }
	#~ close OUT;


	### print the geneList
	open(LIST, ">$FBgn_listname") or die "Cannot open $FBgn_listname!\n";

	foreach my $fbgn (@FBgns) {
		if($fbgn eq "NA") { next }
		print LIST "$fbgn\n";
	}
	close LIST;
	
	### print the geneList
	open(LIST, ">$GeneSymbol_listname") or die "Cannot open $GeneSymbol_listname!\n";

	foreach my $fbgn (@FBgns) {
		if($fbgn eq "NA") { next }
		print LIST "$GeneName{$fbgn}\n";
	}
	close LIST;

	### report on terminal
	print "\tPeaks      :\t$peak_cnt\n";
	print "\tUnique FBgn:\t$unique_cnt\n";
	print "\tNot matched:\t$not_match_cnt\n";

}

print "Done.\n\n";

