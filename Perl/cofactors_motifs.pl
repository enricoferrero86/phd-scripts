#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

use strict;
use warnings;
use Data::Dumper;

unless ($ARGV[0]) {
	print "USAGE: $0 data_table.txt";
	exit;
}

my $input = $ARGV[0];

my %motifs_tmp;
my %motifs;

open FH, "<", $input or die;
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $tf = $tmp[0];
	my $e = $tmp[1];
	
	if ($e < 0.05) {
		$motifs_tmp{$tf}{$e}++;
	}
}

foreach my $tf (keys %motifs_tmp) {
	my $n_tmp;
	my $n;
	my $sum;
	my $avge;
	foreach my $e (keys %{$motifs_tmp{$tf}}) {
		$n_tmp = $motifs_tmp{$tf}{$e};
		$sum += $e;
	}
	$n = $n_tmp * keys %{$motifs_tmp{$tf}};
	$avge = $sum/$n;
	#~ print $tf . "\t" . $n . "\t" . $avge . "\n";
	$motifs{$n}{$avge} = $tf;
}

foreach my $n (sort {$b<=>$a} keys %motifs) {
	foreach my $e (sort keys %{$motifs{$n}}) {
		my $tf = $motifs{$n}{$e};
		print $tf . "\t" . $n . "\t" . $e . "\n";
	}
}

		
	

