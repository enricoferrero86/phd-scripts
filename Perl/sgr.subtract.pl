#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %sgr1;
my %sgr2;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 first_sgr_file.sgr second_sgr_file.sgr > diff_sgr_file.sgr\n";
	exit;
}

my $sgrfile1 = $ARGV[0];
my $sgrfile2 = $ARGV[1];

open my $SGRFILE1, "<", $sgrfile1 or die "Can't open $sgrfile1: $!";
while (my $line = <$SGRFILE1>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $nt = $tmp[1];
	my $score = $tmp[2];
	$sgr1{$chr}{$nt} = $score;
}
close $SGRFILE1;

open my $SGRFILE2, "<", $sgrfile2 or die "Can't open $sgrfile2: $!";
while (my $line = <$SGRFILE2>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $nt = $tmp[1];
	my $score = $tmp[2];
	$sgr2{$chr}{$nt} = $score;
}
close $SGRFILE2;

foreach my $chr (keys %sgr1) {
	foreach my $nt (keys %{$sgr1{$chr}}) {
		my $score = 0;
		my $score1 = $sgr1{$chr}{$nt};
		my $score2 = $sgr2{$chr}{$nt};
		if ($score1 >= 0 && $score2 >= 0) || ($score1 <= 0 && $score2 <= 0) {
			$score = $score1 - $score2;
		}
		else {
			$score = $score1 + $score2;
		} 
		print $chr . "\t" . $nt . "\t" . $score . "\n";
	}
}
