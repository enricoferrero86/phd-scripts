#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless (@ARGV) {
	print "USAGE: $0 bed_file_1.bed bed_file_2.bed ...\n";
	print "You can use as many input bedfiles as you want\n";
	exit;
}

my %int;

foreach my $file (@ARGV) {
	open my $FILE, "<", $file or die "Can't open $file: $!";
	while (my $line = <$FILE>) {
		my @tmp = split /\t/, $line;
		my $chr = $tmp[0];
		my $ntl = $tmp[1];
		my $ntr = $tmp[2];
		foreach my $nt ($ntl .. $ntr) {
			$int{$chr}{$nt}++;
		}
	}
}

foreach my $chr (keys %int) {
	my $max = 0;
	foreach my $nt (keys %{$int{$chr}}) {
		if ($nt > $max) {
			$max = $nt;
		}
	}
	my $flag = 0;
	foreach my $nt (0 .. $max+1) {
		if ($flag == 0) {
			if (exists ($int{$chr}{$nt})) {
				my $ntl = $nt;
				print "$chr\t$ntl\t";
				$flag = 1;
			}
		}
		if ($flag == 1) {
			if (exists ($int{$chr}{$nt})) {
			}
			else {
				my $ntr = $nt-1;
				print "$ntr\n";
				$flag = 0;
			}
		}
	}
}

