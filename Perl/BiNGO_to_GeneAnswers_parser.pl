#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my $c=0;

unless (@ARGV) {
	print "USAGE: $0 BiNGO_Clusters.txt > GeneAnswer_Clusters.R\n";
	exit;
}

print "library(annotate)\n";
print "library(org.Dm.eg.db)\n";
print "library(GeneAnswers)\n";

print "\n\n";

open my $FILE, "<", $ARGV[0] or die "Can't open $ARGV[0]: $!";
while (my $line = <$FILE>) {
	if ($line =~ /^(cluster_\d+)$/) {
		$c++;
		print "$1<-c(";
		next;
	}
	if ($line  =~ /^(batch)$/) {
		print ")\n";
		next;
	}
	if ($line =~ /^(\w+)$/) {
		print "\"$1\",";
	}
}

print "\n\n";

for (my $i=1; $i<=$c; $i++) {
	print "cluster_$i <- as.data.frame(unlist(lookUp(cluster_$i, 'org.Dm.eg', 'ALIAS2EG'),use.names=FALSE))\n";
}

print "\n\n";

for (my $i=1; $i<=$c; $i++) {
	print "colnames(cluster_$i) <- 'GeneID'\n";
}

print "\n\n";

print "clusters <- list(";
for (my $i=1; $i<=$c; $i++) {
	print "cluster_$i=cluster_$i,";
}
print ")\n";

print "\n\n";

print "clusters.GOBP <- lapply(clusters, geneAnswersBuilder, 'org.Dm.eg.db', categoryType='GO.BP', testType='hyperG', pvalueT=0.05, FDR.correction=TRUE, level=3, sortBy='correctedPvalue', verbose=FALSE)\n";
print "\n\n## THIS FAILS HERE IF SOME OF THE CLUSTERS HAVE NO ENRICHMENT ##\n\n";
print "output<- getConceptTable(clusters.GOBP, items='geneNum')\n";
print "drawTable(output[[1]], matrixOfHeatmap=output[[2]], mar=c(2,28,3,2), clusterTable=NULL)\n";
