#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my %scores;

unless ($ARGV[0]) {
	print "USAGE: $0 sgr_file.sgr > density_file.density\n";
	exit;
}

my $sgrfile = $ARGV[0];

open my $SGRFILE, "<", $sgrfile or die "Can't open $sgrfile: $!";
while (my $line = <$SGRFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $ntl = $tmp[1];
	my $score = $tmp[2];
	my $coord;
	my $ntr = $ntl + 25;
	$scores{$chr}{$ntl}{$ntr} = $score;
}
close $SGRFILE;

foreach my $chr (sort {$a cmp $b} keys %scores) {
	foreach my $ntl (sort {$a <=> $b} keys %{$scores{$chr}}) {
		foreach my $ntr (sort {$a <=> $b} keys %{$scores{$chr}{$ntl}}) {
			my $score = $scores{$chr}{$ntl}{$ntr};
				print $chr . "\t" . $ntl . "\t" . $ntr . "\t" . $score . "\n";
		}
	}
}

