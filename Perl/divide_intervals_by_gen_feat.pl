#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


# Here, "exon" refers to all coding exons, but not to exons which form the UTRs!
# UTRs are always made of exons, however it seems useful to distinguish between coding exons and non-coding exons (UTRS).

use strict;
use warnings;
use DBI;

sub run_query {
  my $SQL_statement = $_[0];
  $_[2] = $_[1]->prepare($SQL_statement);
  $_[2]->execute() or die("fucking hell, your SQL was wrong");
}

my $database_handle1 = DBI->connect("DBI:mysql:enrico:127.0.0.1:7777", "enrico", "enricoSQL");
my $st;
my $st2;

my %genic;
my %intergenic;
my %exonic;
my %intronic;
my %utr5;
my %utr3;
my %mixed;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 [gs]bed_file.[gs]bed dataset_name\n";
	exit;
}

my $sbedfile = $ARGV[0];
my $dataset = $ARGV[1];

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	
	my $coord = $chr . "\t" . $start . "\t" . $end;
	
	my $genic_count = 0;
	my $exonic_count = 0;
	my $intronic_count = 0;
	my $utr5_count = 0;
	my $utr3_count = 0;
	
	run_query qq(SELECT chrom, txStart, txEnd, cdsStart, cdsEnd, exonStarts, exonEnds FROM UCSC_dm3.flyBaseGene WHERE chrom="$chr" AND ($nt BETWEEN txStart AND txEnd)), $database_handle1, $st2;	
	while (my ($chrom, $txStart, $txEnd, $cdsStart, $cdsEnd, $exonStarts, $exonEnds) = $st2->fetchrow_array) {

		my $utr5Start = $txStart;
		my $utr5End = $cdsStart;
		my $utr3Start = $cdsEnd;
		my $utr3End = $txEnd;
		
		my $exonic_temp = 0;
		my $intronic_temp = 0;
		my $utr5_temp = 0;
		my $utr3_temp = 0;
		
		$genic_count++;
		
		my @exon_starts = split(/,/, $exonStarts);
		my @exon_ends = split(/,/, $exonEnds);
		
		foreach my $i (0 .. $#exon_starts) {
			if ($exon_starts[$i] <= $nt and $nt <= $exon_ends[$i]) {
				$exonic_temp++;
			}
			if ($exon_ends[$i] <= $nt and $nt <= $exon_starts[$i+1]) {
				$intronic_temp++;
			}
		}
		
		if ($utr5Start <= $nt and $nt <= $utr5End) {
			$utr5_temp++;
		}
		
		if ($utr3Start <= $nt and $nt <= $utr3End) {
			$utr3_temp++;
		}
		
		
		if ($exonic_temp > 0) {
			$exonic_count++;
		}
		if ($intronic_temp > 0) {
			$intronic_count++;
		}
		if ($utr5_temp > 0) {
			$utr5_count++;
		}
		if ($utr3_temp > 0) {
			$utr3_count++;
		}
	}
	
	
	if ($genic_count > 0) {
		
		$genic{$coord}++;
		
		if ($exonic_count > 0 and $intronic_count == 0 and $utr5_count == 0 and $utr3_count == 0) {
			$exonic{$coord}++;
		}
		elsif ($exonic_count == 0 and $intronic_count > 0 and $utr5_count == 0 and $utr3_count == 0) {
			$intronic{$coord}++;
		}
		elsif ($exonic_count > 0 and $intronic_count == 0 and $utr5_count > 0 and $utr3_count == 0) {
			$utr5{$coord}++;
		}
		elsif ($exonic_count > 0 and $intronic_count == 0 and $utr5_count == 0 and $utr3_count > 0) {
			$utr3{$coord}++;
		}
		else {
			$mixed{$coord}++;
		}
	}
	
	else {
		$intergenic{$coord}++;
	}
}		

		
&write_to_file ($dataset, "genic", %genic);
&write_to_file ($dataset, "intergenic", %intergenic);
&write_to_file ($dataset, "exonic", %exonic);
&write_to_file ($dataset, "intronic", %intronic);
&write_to_file ($dataset, "utr5", %utr5);
&write_to_file ($dataset, "utr3", %utr3);
&write_to_file ($dataset, "mixed", %mixed);


sub write_to_file  {
	my ($dataset, $gen_feat, %hash) = @_;
	my $bedfile = "${dataset}_${gen_feat}.bed";
	open my $BEDFILE, ">", $bedfile or die "Can't open $bedfile: $!";
	foreach my $coord (keys %hash) {
		print $BEDFILE "$coord\n";
	}
	close $BEDFILE;
}

