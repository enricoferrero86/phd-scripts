#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0]) {
	print "USAGE: $0 fastafile.fasta\n";
	exit;
}

my $fastafile = $ARGV[0];
my %fastaseqs;
my $header;
my $pattern = "[AT][AT]CAA[AT]G";
my $matches;
my %matchseqs;
my $total_matches;

open FASTAFILE, $fastafile or die "Can't open $fastafile: $!";

while (my $fastaline = <FASTAFILE>) {
	chomp $fastaline;
	if ($fastaline =~ /^\s*$/) {
		next;
	}
	elsif ($fastaline =~ /^>/) {
		$header = $fastaline;
		}
	else {
		$fastaseqs{$header} .= $fastaline;
		}
}

foreach (keys %fastaseqs) {
	if ($fastaseqs{$_} =~ /$pattern/i) {
		$matches++;
		$matchseqs{$_} = $fastaseqs{$_};
	}
	while ($fastaseqs{$_} =~ /$pattern/ig) {
		$total_matches++;
	}
}

my $total_seqs = keys %fastaseqs;
my $perc = ($matches / $total_seqs) *100;

printf "\nIn $fastafile, '$pattern' occurs $total_matches times, in $matches out of $total_seqs intervals (%.2f%%).\n\n", $perc;

print "These are the intervals where '$pattern' was found:\n\n";
foreach (sort keys %matchseqs) {
	print "$_\n$matchseqs{$_}\n";
}
