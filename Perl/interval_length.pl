#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my @lengths;
my $sum;
my $avg;
my $perc;
my $genome = 120000000;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 bedfile.bed dataset\n";
	exit;
}

my $bedfile = $ARGV[0];
my $dataset = $ARGV[1];

open my $BEDFILE, "<", $bedfile or die "Can't open $bedfile: $!";
while (my $line = <$BEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $length = $end - $start + 1;
	push (@lengths, $length);
	}


foreach my $length (@lengths) {
	$sum += $length;
}

# Average length
$avg = $sum / ($#lengths + 1);
printf "#The average length of intervals in %s is %.2f.\n", $bedfile, $avg;

# Percentage of genome covered
$perc = ($sum / $genome) * 100;
printf "#The percentage of genome covered in %s is %.2f\%.\n", $bedfile, $perc;

# This variable define the separator in the array, very useful for printing!
$"=", ";
print "$dataset <- c(@lengths)\n\n";

