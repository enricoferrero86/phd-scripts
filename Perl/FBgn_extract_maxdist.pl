#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

my $maxdist;
my %genes;
my %intervals;
my %genehits;
my %unresolved;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 bed_file.bed flymine_ref_file.tsv [maximum distance]\n";
	print "INFO: maximum distance default is 10 kb\n";
	exit;
}
my $bedfile = $ARGV[0];
my $reffile = $ARGV[1];

if ($ARGV[2]) {
	$maxdist = $ARGV[2];
}
else {
	$maxdist = 10000;
}

open BEDFILE, $bedfile or die "Can't open $bedfile: $!";
while (my $bedline = <BEDFILE>) {
	chomp $bedline;
	my @tmp = split /\t/, $bedline;
	my $bedchr = $tmp[0];
	my $bedstart = $tmp[1];
	my $bedend = $tmp[2];
	my $bedcoord = $bedstart . "\t" . $bedend;
	$intervals{$bedchr}{$bedcoord}++;
}
close BEDFILE;

open REFFILE, $reffile or die "Can't open $reffile: $!";
readline(REFFILE);
while (my $refline = <REFFILE>) {
	chomp $refline;
	my @tmp = split /\t/, $refline;
	my $refchr = "chr" . $tmp[0];
	my $refstart = $tmp[2];
	my $refend = $tmp[3];
	my $fbgn = $tmp[7];
	my $refcoord = $refstart . "\t" . $refend;
	$genes{$refchr}{$fbgn} = $refcoord;
}
close REFFILE;

foreach my $chr (keys %intervals) {
	
	foreach my $coord (keys %{$intervals{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $flag = 0;
		
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};

			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
		
			if (($intstart >= $genestart and $intstart <= $geneend) or
				($intend >= $genestart and $intend <= $geneend) or
				($genestart >= $intstart and $genestart <= $intend) or
				($geneend >= $intstart and $geneend <=$intend)) {
				$genehits{$fbgn}++;
				$flag++;
			}
		}
			
		if ($flag == 0) {
			$unresolved{$chr}{$coord}++;
		}
	}
}

foreach my $chr (keys %unresolved) {
	
	foreach my $coord (keys %{$unresolved{$chr}}) {
		my @tmp = split /\t/, $coord;
		my $intstart = $tmp[0];
		my $intend = $tmp[1];
		my $candidate = "none";
		my $dist = $maxdist;
		
		foreach my $fbgn (keys %{$genes{$chr}}) {
			my $coord = $genes{$chr}{$fbgn};
			my @tmp = split /\t/, $coord;
			my $genestart = $tmp[0];
			my $geneend = $tmp[1];
			
			# gene upstream of interval
			if ($geneend < $intstart) {
				my $genedist = $intstart - $geneend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
			# gene downstream of interval
			if ($genestart > $intend) {
				my $genedist = $genestart - $intend;
				if ($genedist <= $dist) {
					$dist = $genedist;
					$candidate = $fbgn;
				}
			}
		}
		unless ($candidate eq "none") {
			$genehits{$candidate}++;
		}
	}
}

foreach my $fbgn (keys %genehits) {
	print $fbgn, "\n";
}					


