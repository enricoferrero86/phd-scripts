#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0] and $ARGV[1]) {
	print "USAGE: $0 cytoscape_network.sif gene_list.txt > final_network.sif\n";
	exit;
}

my $network_file = $ARGV[0];
my $gene_list = $ARGV[1];

my @genes;

open FH, "<", $gene_list or die;
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $gene = $tmp[0];
	push @genes, $gene;
}
close FH;

open FH, "<", $network_file or die;
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $gene1 = $tmp[0];
	my $interaction = $tmp[1];
	my $gene2 = $tmp[2];
	foreach my $gene (@genes) {
		if ($gene eq $gene1) {
			foreach my $gene (@genes) {
				if ($gene eq $gene2) {
					print $gene1."\t".$interaction."\t".$gene2."\n";
				}
			}
		}
	}
}
close FH;

	
