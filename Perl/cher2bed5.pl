#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0]) {
	print "USAGE: $0 Ringo_cher_file.txt > scores_bed_file.bed5\n";
	exit;
}

my $cherfile = $ARGV[0];
(my $name = $cherfile) =~ s/(.+?)\..+/$1/; 

open my $CHERFILE, "<", $cherfile or die "Can't open $cherfile: $!";
readline($CHERFILE);
while (my $line = <$CHERFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $maxLevel = $tmp[3];
	my $score = $tmp[4];
	my $probes = $tmp[5];
	my $avgscore = $score/$probes;
	print $chr . "\t" . $start . "\t" . $end . "\t" . $name . "\t" . $avgscore . "\n";
}
close $CHERFILE;
