#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;
use Data::Dumper;

unless (scalar(@ARGV) == 3) {
	print "USAGE: $0 FlyMine_intron_ref.tsv TF_sbedfile.sbed dataset_name\n";
	exit;
}

my $reffile = $ARGV[0];
my $sbedfile = $ARGV[1];
my $data = $ARGV[2];

my %trs;
my %l_trs_tmp;
my %l_trs;

my %all;
my %bound;


open FH, "<", $reffile or die "Can't open $reffile: $!";
readline(FH);
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $gn = $tmp[0];
	my $tr = $tmp[1];
	my $tr_length = $tmp[2];
	my $chr = "chr" . $tmp[3];
	my $strand = $tmp[4];
	my $start = $tmp[5];
	my $end = $tmp[6];
	my $int_length = $tmp[7];
	
	my $int_coord = $start . "\t" . $end;
	$trs{$gn}{$tr}{$tr_length}{$chr}{$int_coord} = $int_length;
}
close FH;

foreach my $gn (keys %trs) {
	my $l_tr = "";
	my $l_tr_length = 0;
	foreach my $tr (keys %{$trs{$gn}}) {
		foreach my $tr_length (keys %{$trs{$gn}{$tr}}) {
			if ($tr_length > $l_tr_length) {
				$l_tr = $tr;
				$l_tr_length = $tr_length;
			}
		}
	}
	$l_trs_tmp{$gn}{$l_tr}{$l_tr_length}++;
}

foreach my $gn (keys %l_trs_tmp) {
	foreach my $tr (keys %{$l_trs_tmp{$gn}}) {
		foreach my $tr_length (keys %{$l_trs_tmp{$gn}{$tr}}) {
			foreach my $chr (keys %{$trs{$gn}{$tr}{$tr_length}}) {
				foreach my $coord (keys %{$trs{$gn}{$tr}{$tr_length}{$chr}}) {
					my $int_length = $trs{$gn}{$tr}{$tr_length}{$chr}{$coord};
					$l_trs{$gn}{$tr}{$tr_length}{$chr}{$coord} = $int_length;
					$all{$gn} = $int_length;
				}
			}
		}
	}
}


open FH, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <FH>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	
	foreach my $gn (keys %l_trs) {
		foreach my $tr (keys %{$l_trs{$gn}}) {
			foreach my $tr_length (keys %{$l_trs{$gn}{$tr}}) {
				foreach my $int_chr (keys %{$l_trs{$gn}{$tr}{$tr_length}}) {
					foreach my $int_coord (keys %{$l_trs{$gn}{$tr}{$tr_length}{$int_chr}}) {
						my $int_length = $l_trs{$gn}{$tr}{$tr_length}{$int_chr}{$int_coord};
						my @tmp = split /\t/, $int_coord;
						my $int_start = $tmp[0];
						my $int_end = $tmp[1];
						
						if ($chr eq $int_chr) {
							if ($nt > $int_start and $nt < $int_end) {
								$bound{$gn} = $int_length;
							}
						}
					}
				}
			}
		}
	}
}


print "${data}_ALL <- c(";
foreach my $gn (keys %all) {
	my $length = $all{$gn};
	print "$length, ";
}
print ")\n";

print "${data}_BOUND <- c(";
foreach my $gn (keys %bound) {
	my $length = $bound{$gn};
	print "$length, ";
}
print ")\n";

print "${data}_UNBOUND <- ${data}_ALL[-${data}_BOUND]\n";




