#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.

use strict;
use warnings;

my %intervals;

unless ($ARGV[0]) {
	print "USAGE: $0 sbed_file.sbed [number of intervals]\n";
	exit;
}

my $sbedfile = $ARGV[0];

open my $SBEDFILE, "<", $sbedfile or die "Can't open $sbedfile: $!";
while (my $line = <$SBEDFILE>) {
	chomp $line;
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $nt = $tmp[3];
	my $score = $tmp[4];
	my $coord = $start . "\t" . $end;
	$intervals{$score}{$chr}{$coord} = 1;
}
close $SBEDFILE;

my @scores = sort {$b <=> $a} keys %intervals;

if ($ARGV[1]) {
	my $number = $ARGV[1];
	my @topscores = @scores[0 .. $number-1]; # array slice!
	my @otherscores = @scores[$number .. $#scores];
	
	(my $topbedfile = $sbedfile) =~ s/(.+)\.sbed/$1_TOP$number\.bed/;
	open my $TOPBEDFILE, ">", $topbedfile or die "Can't open $topbedfile: $!";
	foreach my $score (@topscores) {
		foreach my $chr (keys %{$intervals{$score}}) {
			foreach my $coord (keys %{$intervals{$score}{$chr}}) {
				print $TOPBEDFILE $chr . "\t" . $coord . "\n";
			}
		}
	}
	close $TOPBEDFILE;
	
	(my $otherbedfile = $sbedfile) =~ s/(.+)\.sbed/$1_OTHER$number\.bed/;
	open my $OTHERBEDFILE, ">", $otherbedfile or die "Can't open $otherbedfile: $!";
	foreach my $score (@otherscores) {
		foreach my $chr (keys %{$intervals{$score}}) {
			foreach my $coord (keys %{$intervals{$score}{$chr}}) {
				print $OTHERBEDFILE $chr . "\t" . $coord . "\n";
			}
		}
	}
	close $OTHERBEDFILE;
}

else {	
	(my $rankedbedfile = $sbedfile) =~ s/(.+)\.sbed/$1_RANKED\.bed/;
	open my $RANKEDBEDFILE, ">", $rankedbedfile or die "Can't open $rankedbedfile; $!";
	foreach my $score (@scores) {
		foreach my $chr (keys %{$intervals{$score}}) {
			foreach my $coord (keys %{$intervals{$score}{$chr}}) {
				print $RANKEDBEDFILE $chr . "\t" . $coord . "\n";
			}
		}
	}
	close $RANKEDBEDFILE;
}


