#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero, Bettina Fischer.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# Original code by Bettina Fischer, modified by Enrico Ferrero.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;


my %genome_sequence = ();
# load genomic sequences into memory -- warning: don't try this at home
# on hal/thebeast/themonster:
my $PATH2SEQ = "/flychip/share/Genomes/UCSCmirror/dm3/bigZip/";

my @chrom = ("chr2L", "chr2LHet", "chr2R", "chr2RHet", "chr3L", "chr3LHet", "chr3R", "chr3RHet", "chr4", "chrX", "chrXHet", "chrYHet", "chrM", "chrU", "chrUextra");

foreach my $chr (@chrom) {
	open FILE, $PATH2SEQ.$chr.".fa";
	while (defined (my $line = <FILE>)) {
		chomp $line;
		if ($line !~ /\>/) {
			$genome_sequence{$chr} .= $line;
	  	}
  	}
	close FILE;
}
  
open HANDLE, $ARGV[0];

my $count = 0;

while (defined (my $line = <HANDLE>)) {
    chomp $line;
    $count++;
    my @tmp = split(/\t/, $line);
    
    my $chr = $tmp[0];
    my $ntl = $tmp[1];
    my $ntr = $tmp[2];
  
    
     #...and extract the underlying genome sequence and print it in FASTA format
	my $seq = substr($genome_sequence{$chr}, $ntl, $ntr-$ntl);
	print ">$chr:$ntl-$ntr\n$seq\n";
}
  
close HANDLE;
