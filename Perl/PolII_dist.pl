#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless (scalar(@ARGV) == 3) {
	print "USAGE: $0 TF_bedfile.bed PolII_bedfile.bed dataset_name > results.R\n";
	exit;
}

my $TF_file = $ARGV[0];
my $polII_file = $ARGV[1];
my $data = $ARGV[2];

my %TF;
my $range = 1000;
my $dist = 0;

open FILE, "<", $TF_file or die "Can't open $TF_file: $!";
while (my $line = <FILE>) {
	my @tmp = split /\t/, $line;
	my $chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $midnt = int (($start + $end) / 2);
	$TF{$chr}{$midnt}++;
}
close FILE;

print "$data <- c(";
open FILE, "<", $polII_file or die "Can't open $polII_file: $!";
while (my $line = <FILE>) {
	my @tmp = split /\t/, $line;
	my $polII_chr = $tmp[0];
	my $start = $tmp[1];
	my $end = $tmp[2];
	my $midnt = int (($start + $end) / 2);
	my $l_limit = $midnt - $range;
	my $r_limit = $midnt + $range;
	
	foreach my $chr (keys %TF) {
		if ($chr eq $polII_chr) {
			foreach my $nt (keys %{$TF{$chr}}) {
				if (($nt > $l_limit) and ($nt < $r_limit)) {
					if ($nt < $midnt) {
						$dist = $midnt - $nt;
					}
					else {
						$dist = $nt - $midnt;
					}
					print "$dist, ";
				}
			}
		}
	}
}
close FILE;
print ")\n\n";
