#!/usr/bin/perl

# Copyright (C) 2010-2013 Enrico Ferrero.
# Email: <enricoferrero86 [at] gmail [dot] com>.
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details: <http://www.gnu.org/licenses/gpl.txt>.


use strict;
use warnings;

unless ($ARGV[0]) {
	die "USAGE: $0 widgetbdgp_enrichment.tsv > BDGP_enrichment.txt";
}

open FILE, $ARGV[0] or die "Can't open $ARGV[0]";
while (<FILE>) {
	my @tmp = split /\t/;
	my $BDGP_term = $tmp[0];
	my $pvalue = $tmp[1];
	printf "%s\t%.2e\n", $BDGP_term, $pvalue;
}
close FILE;


